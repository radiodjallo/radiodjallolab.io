---
layout: page
title: Low.Tech.Web
---
----
<br/>
## **Ossia perché questo sito è così semplice, perché le immagini sembrano rovinate e perché tra poco potremo crescere frutta tropicale in Piemonte.**
<br/>
Ok gente, facciamola breve: moriremo tutte.

Nel senso, di certo prima o poi moriremo tutti, ma per come stanno messe le cose oggi, è altamente probabile che nel prossimo secolo si muoia tutti, più o meno lentamente, come specie umana a causa di variegati e devastanti eventi climatici catastrofici con relative conseguenze.

Insomma, ormai dovremmo saperlo: il cambiamento climatico determinato dalle attività produttive umane è una realtà certa, con conseguenze certe e certamente devastanti (per noi) e il tempo a nostra disposizione per tentare di cambiare le cose e salvarci è ormai poco. Dato che il tempo è già poco, noi risparmiamo il nostro nel riportare informazioni [che altri hanno già riportato meglio di quanto potremmo mai fare noi.](https://storie.valigiablu.it/climatechange/)  


Ora, questa breve premessa risponde in realtà a ritroso alla domanda: ci spiega insomma perché a breve potremmo crescere la frutta tropicale in Piemonte, ma non fornisce chiarimenti sulle altre due questioni.

Perché questo sito è così semplice? Quali problemi di vista vi hanno portato a caricare immagini così malamente trattate?
Premettendo che uno di noi, è vero, porta gli occhiali, non sono stati i problemi di vista a guidare le nostre scelte progettuali ed estetiche durante la realizzazione di questo sito.

Cosa quindi?

La risposta si trova proprio nella premessa principale: il cambiamento climatico.

Internet è *virtuale* fino a un certo punto: dietro le nostre bellissime pagine, le serie in streaming, le piattaforme social, dietro questa facciata così *soft* e malleabile capace di fornirci contentuti interattivi e in continua evoluzione, dietro tutto questo c'è una realtà decisamente *hard*. Ci sono luoghi che contengono computer che contengono le serie trasmesse sul nostro schermo quando clicchiamo "Play"; o altri luoghi, con altri computer, che contengono tutte le nostre mail, i nostri profili social, le nostre foto online; ci sono cavi che collegano i computer di questi luoghi alle nostre città, fino ad arrivare ai cavi che portano la rete dentro il nostro router e infine dentro i nostri computer; e c'è, infine, anzi in origine, *corrente*. 

Elettricità, corrente elettrica, energia elettrica: chiamatela come volete ma il fatto rimane uguale. Il nostro vaso di Pandora virtuale per rimanere *online* ha bisogno di carburante, e questo carburante è l'energia elettrica. Ma da dove arriva l'energia elettrica? Dalle turbine eoliche, dai pannelli solari, dalle centrali idroelettriche... in parte ridotta, mentre la parte maggiore arriva [dalla combustione di carbone e gas naturale.](https://theshiftdataportal.org/energy/electricity?chart-type=stacked&chart-types=stacked&chart-types=stacked-percent&chart-types=pie&chart-types=line&chart-types=ranking&disable-en=false&ef-generation=Oil&ef-generation=Coal&ef-generation=Gas&ef-generation=Nuclear&ef-generation=Hydro&ef-generation=Wind&ef-generation=Biomass&ef-generation=Waste&ef-generation=Solar%20PV&ef-generation=Geothermal&ef-generation=Solar%20Thermal&ef-generation=Tide&ef-capacity=Fossil%20Fuels&ef-capacity=Hydroelectricity&ef-capacity=Nuclear&ef-capacity=Hydroelectric%20Pumped%20Storage&ef-capacity=Wind&ef-capacity=Solar%2C%20Tide%2C%20Wave%2C%20Fuel%20Cell&ef-capacity=Biomass%20and%20Waste&ef-capacity=Geothermal&energy-unit=TWh&group-names=World&is-range=true&gdp-unit=GDP%20(constant%202010%20US%24)&type=Generation&dimension=byEnergyFamily&end=2015&start=1990&multi=false)   

Quindi, se oggi l'energia elettrica viene prodotta principalmente utilizzando combustibili fossili, qualunque cosa ne faccia uso in qualche modo contribuisce alle emissioni climalteranti. *Anche internet.* In che misura, ossia quale sia l'effettivo consumo elettrico dell'internet, [pare essere ancora poco chiaro](https://solar.lowtechmagazine.com/2015/10/can-the-internet-run-on-renewable-energy.html) , ma un consumo esiste ed è innegabile.

*"Beh, basterebbe alimentare ogni dispositivo e infrastruttura che richiede energia elettrica con energia prodotta da fonti rinnovabili e il problema sarebbe risolto!"*

Già.
Peccato che la transizione verso una produzione elettrica completamente derivante da energie rinnovabili sia ancora lontana, e lo sia anche perché, in generale, consumiamo troppo. [Internet inclusa.](https://solar.lowtechmagazine.com/2015/10/can-the-internet-run-on-renewable-energy.html)   

Ora, prima che iniziate ad alzare le forche nei nostri confronti accusandoci di essere degli ipocriti decrescisti comunisti depravati che pensano che la rete così come l'abbiamo oggi non sia sostenibile, sebbene avreste ragione su ogni termine, aspettate un attimo. Così almeno possiamo finalmente dare una risposta alla domanda iniziale: *perché questo sito è così semplice e perché le immagini sembrano rovinate?*

Abbiamo detto che internet, ovviamente, consuma energia. Ma di preciso, *cosa* consuma energia? I fattori sono molteplici: il server che contiene le informazioni consuma energia per stare acceso, il nostro pc anche, stessa cosa il nostro router, così come tutti gli altri elementi dell'infrastruttura, sui quali abbiamo possiamo avere più o meno controllo e che non approfondiremo qui. C'è un aspetto che invece vogliamo approfondire per rispondere alla nostra domanda: lo scambio di dati.

Quando mi collego a una pagina web, quando clicco "play" su un video in streaming, dei dati contenuti su un computer lontano (il server) vengono trasmessi al mio pc, che li interpreta e li mostra sullo schermo. Questo processo di *download* e *upload* ovviamente implica un consumo di energia, proporzionale ovviamente alla quantità di dati che vengono trasmessi.

Immaginiamo di dover prendere in prestito un libro da un nostro amico o amica: se si tratta di un opuscolo di 10 pagine spenderemo sicuramente meno energia per trasportarlo fino a casa nostra nel nostro zaino, rispetto al caso in cui il libro in questione fosse una versione originale dell'enciclopedia avente 400 pagine e dimensioni di 1x1,5 metri.

Ecco, con la rete è la stessa cosa: *scaricare* (ossia trasportare dal server al nostro pc) un file da 1 megabyte ovviamente richiederà molta meno energia rispetto a scaricarne uno da 100.

Le pagine web sono la stessa cosa: ogni volta che apriamo un sito, i dati che compongono le immagini, i video, i testi, gli elementi interattivi e tutto il resto vengono trasportati dai server al nostro pc. Più dati sono, più si consuma energia. Più si consuma energia, più si contribuisce a emettere gas climalteranti nell'atmosfera. Come accennavamo inoltre, più consumiamo e più diventa difficile potersi affidare alle fonti rinnovabili, che sono per natura discontinue.

*"Che fare?"*, quindi, come disse [uno dei più grandi uomini della storia?](https://it.wikipedia.org/wiki/Lenin)    

Possiamo ridurre i consumi. **Ed è per questo che il sito è così semplice e le immagini sembrano rovinate.**

Oggi la pagina web media è molto pesante, sicuramente più pesante di quanto realmente necessario. Video, immagini, ma non solo: pubblicità, elementi traccianti che seguono i nostri comportamenti virtuali per proporci annunci e inserzioni, tutti questi elementi fanno sì che ogni volta che una pagina si apre nel nostro browser moltissimi dati vengano trasportati *avanti e indietro* dal server al nostro pc e viceversa. Dati superflui, se non addirittura [dannosi per noi e la nostra privacy.](https://www.freecodecamp.org/news/what-you-should-know-about-web-tracking-and-how-it-affects-your-online-privacy-42935355525/)  

[Informandoci](http://gauthierroussilhe.com/en/posts/convert-low-tech)   attraverso [diverse guide](https://solar.lowtechmagazine.com/2015/10/how-to-build-a-low-tech-internet.html) abbiamo costruito questo sito web cercando quanto possibile di limitare i consumi. Il sito è statico, non contiene script o elementi traccianti che aumentano inutilmente il traffico di dati; le immagini vengono sottoposte a trattamenti grafici che ne riducono moltissimo le dimensioni rispetto al file originale in modo da ridurre il trasferimento di dati. Linkeremo comunque ogni cover degli album all'immagine originale non trattata: per vederla vi basterà quindi cliccare, ma vi consigliamo di farlo (e di avviare quindi un trasferimento dati più consistente) solo nel caso in cui davvero vi interessasse. Anche le diverse tipologie di caratteri usati nelle pagine web sono dati da trasferire: per questo il sito viene visualizzato con dei font che avete già sul vostro computer, evitando di doverne scaricare di nuovi e riducendo quindi lo scambio di dati.



*"Tutto sto pippone per cosa? Se davvero aveste voluto evitare di consumare energia, avreste evitato di fare l'ennesimo sito web!"*

Si, era una delle strade possibili. Ma abbiamo preferito sceglierne una che raccontasse come le nostre passioni e l'ambiente **possano conciliarsi**: questo mondo ci ha dato la vita e ci ha ospitato fino ad oggi, e continuerà a farlo se sapremo trovare la giusta misura. 

Non un rinunciare assoluto, non un eccedere miope e sconsiderato: un vivere rispettosi degli equilibri di quanto ci circonda.

O così, o estinti :) .