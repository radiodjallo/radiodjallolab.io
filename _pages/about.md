---
layout: page
title: Chi.Siamo
---
----
<span class="didascalia">Tutti i riferimenti a persone che trovere in questo sito verranno declinati alternando aleatoriamente genere femminile e maschile.<br/> Questo per rispettare la parità di genere e il nostro essere aleatori.</span>
<br/>
<br/>
# **Cos'è Radio.Djallo** _(La D è muta, bifolco)_
<br/>
La musica è forse l’unica forma d’arte dalla quale è impossibile sfuggire.  

A differenza delle arti visive, della musica fruiamo anche nostro malgrado, appena entriamo in un negozio, quando la sera andiamo in un pub o accendiamo la radio. In mezzo a questa moltitudine di stimoli è difficile identificare e coltivare un gusto proprio, che ci appartenga. Paradossalmente, in un mondo dove la produzione e la condivisione musicale è alla portata di tutti, è difficile trovare il tempo e la voglia di affrontare le ore di ricerca e di ascolto necessarie per scovare un brano, un album, un artista o un genere che davvero ci tocca e ci trasmette qualcosa di profondo, qualcosa che davvero apprezziamo. 

L’apprezzamento musicale è un meccanismo davvero complesso, influenzato da molti fattori: dov’eri quando hai sentito per la prima volta *quel* pezzo? Cosa facevi? Lo associ a qualcosa, o a qualcuno? Lo hai amato subito o hai imparato ad apprezzarlo con il tempo? Noi pensiamo che l’apprezzamento musicale sia imprescindibile dal contesto, sia quello che ci ha avvicinati a una musica, sia quello che ha permesso a quella musica di esistere. Come quando un amico ti parla con entusiasmo di un film, sarai più suscettibile, se non di amarlo, almeno di guardarlo con più attenzione e trovarlo interessante. Questo è l’obiettivo di questo sito. 

Parleremo di musica.

Della musica che ci piace, di quella che non ci piace, di quella che consigliamo invitando tutti ad ascoltare con maggiore attenzione e coinvolgimento, nella speranza che forse, grazie a noi, qualcuno possa scoprire qualcosa che gli corrisponda, scoprendo qualcosa in più di sè stesso.
<br/>
<br/>
<br/>  

****
# **Ciao, noi siamo lo studio.Djallo e siamo in due** _(E sì, la D è sempre muta, bifolca)_
<br/>
## Luca.Djallo  <span class="didascalia">*// Grafico e Stampatore // Brighton*</span>
La prima musica che ho ascoltato era quella che avevo in casa, come tutti. Il primo album per cui gattoni verso lo stereo, indichi le casse e chiedi “Papà, questo cos’è” è indimenticabile. Ho passato i primi anni di ascolto musicale a passare in loop gli stessi tre dischi: Puta’s Fever di Manonegra, Sheik Yerbouti di Zappa e la Bossa Nova di Quincy Jones.

Quando ho avuto il mio primo lettore MP3, qualcuno mi ha passato il suo gruzzolo prezioso di pochi brani scaricati su e-Mule. Era principalmente pop di quel periodo, Shakira, Britney Spears, Jay-Z, Madonna, Eiffel 65, e anche qualcosa di più vecchio, tipo Celentano e i Beatles. Neanche la nostalgia della gioventù innocente mi impedisce di rinnegare tutto in blocco.

Da adolescente ascoltavo la musica come tutti gli adolescenti: ossessivamente. Il numero di volte che ho ascoltato What I’ve Done dei Linkin Park o Wanderlust King dei Gogol Bordello rimarrà per sempre tra me e l’allora neonato Youtube.

Frequentavo il liceo artistico, andavo alle manifestazioni e al pub a prendere le prime sbronze imbarazzanti e a giocare a giochi di società, quindi i miei gusti oscillavano tra il pop rock dei Green Day, il Punk Rock degli Offspring e lo Ska Punk dei Talco, e in genere disprezzavo qualunque forma di musica elettronica.

All’università sono successe diverse cose importanti: ho conosciuto Marco Djallo, ho fatto la pace con l’elettronica e ho scoperto l’Hip Hop. In questo periodo, con artisti come Gramatik, Chinese man e Ratatat ho iniziato a formare il mio pantheon musicale e a definire il mio gusto attuale. Mi sono aperto a un nuovo tipo di ascolto della musica che ragiona per album invece per singoli brani, e ho iniziato a fare ricerca.

Poi i viaggi che ho fatto e le persone che ho incontrato mi hanno portato a scoprire che l’apprezzamento musicale non è innato: è un percorso lento ma gratificante.
<br/>
<br/>
<br/>
## Marco.Djallo  <span class="didascalia">// Social Designer e Grafico // Torino</span>
La mia famiglia non è mai stata caratterizzata da un forte (ma neanche moderato) interesse per la musica: di conseguenza, le prime note con cui entro in contatto sono quelle del pop italiano e qualche volta internazionale di fine '90 e primi '00 che ascoltavo insieme a mia sorella e vedevo passare su MTV: gruppi come i Cartoons, AQUA e i nostrani Lunapop e 883, di cui conoscevo a memoria le canzoni.

Crescendo, ben prima che alla musica, mi appassiono ai computer: da buon nerd, succede diverse volte che lo strumento arrivi prima del contenuto con cui nutrirlo. Scoprii questo player musicale per il pc di cui mi piaceva l'interfaccia, ma ovviamente un player senza file musicali è poco utile: per quanto l'interfaccia mi piacesse, rimanere a fissarla per ore mi sembrava poco interessante come attività, così iniziai a trovare qualche file da fargli suonare. 
Nasce così la mia prima playlist personale: un calderone di pezzi pop e super hit del tempo e del passato, con pezzi che andavano da Anastacia al remix ad opera di Junkie XL di "A little less conversation" di Elvis, passando per il tema di Rocky e i Queen, tutti scaricati in modo improbabile da non so più neanche quale software P2P probabilmente ora deceduto.
Stupendo.
Ovviamente, sviluppo rapidamente il desiderio di avere sempre con me cotanta bellezza sonora: desiderio che si concretizza in un bellissimo lettore cd-mp3 della Philips, che voglio qui ricordare avendolo bruciato durante qualche esperimento elettronico mal riuscitomi. RIP.

Intanto le scuole medie fanno il loro corso, mia sorella parte per l'Erasmus e iniziano le mie incursioni tra i suoi CD, i quali da questo momento in poi inizieranno lentamente a migrare in modo assolutamente autonomo verso la mia camera. Tra i vari dischi presenti, una sola tipologia poteva essere la prima ad attrarre un ragazzino iperattivo, arrabbiato e cresciuto senza particolare educazione musicale: fu così che mi si spalancarono le porte del Punk Rock.
NoFX, Offspring, Punkreas, Pornoriviste e la perla assoluta che ancora oggi riascolto periodicamente: la compilation Punk-O-Rama Volume 6 della Epitaph.
Intanto, il lettore cd si trasforma in un piccolo ma solido lettore MP3 della Thompson. Bellissimo oggetto.

Non l'ho ancora capito, ma la musica è diventata qualcosa di serio, molto serio. E infatti, arriva il periodo delle svolte.

Io e Ricky ci conosciamo nei parchetti sotto casa: qualche settimana ed è subito associazione a delinquere. Rik ha i capelli lunghi, si porta sempre in giro una chitarra classica e sta in fissa con i Red Hot Chili Peppers. Di lì a poco, sarò anche io in fissa con i RHCP, nelle mie mani sarà apparso un basso elettrico Washburn bianco ma i capelli rimarranno corti, anzi cortissimi: un mohicano. I due improbabili soggetti recuperano un terzo amico, batterista, compagno di scuola di Ricky e nascono i Green Heaven, che presero il nome da un pezzo del primo disco dei Red Hot.

Cambio di piano: ora la musica la faccio (male), non mi limito ad ascoltarla.

Da quel momento è un esplosione: al Punk Rock si affianca il vecchio, sano Hard Rock che accompagna i primi passi di chiunque prenda uno strumento in mano: arrivano i nomi sacri. Led Zeppelin, The Who (da bassista, come potevo ignorare John Entwistle?), Black Sabbath (che segneranno una svolta nel mio modo di scrivere i testi), AC/DC (ora mi è impossibile ascoltarli.), Aerosmith e poi loro, i Guns 'N Roses. Brividi.

Suona&Ascolta&Suona&Ascolta&Suona&Ascolta, il panorama si amplia in fretta: arriva qualcosa di più pesante (Metallica, Iron Maiden), ma soprattutto di più moderno e contaminato: Limp Bizkit e Audioslave, i quali mi apriranno le porte verso i Rage Against The Machine. Primi sprazzi di rap, mentre ignaro mi avvicino a una nuova svolta.

I Green Heaven, per essere un gruppo di scombinati autodidatti, procedono bene: le idee ci sono e la *fotta* pure. Ma, come spesso accade in queste situazioni, qualcosa cambia e cambia anche il gruppo. Nucleo originario intatto, arriva un nuovo batterista. Suona con le bacchette, e con la punta delle dita: il ragazzo è anche beatmaker, ed è fottutamente bravo. In entrambi i campi.

Un'ondata di funk travolge me e Rik, ma la vera sberla ce la dà l'apertura del vaso del rap. Italiano o americano, tutto ci colpisce all'improvviso e insieme: Kaos One, Neffa e i Messaggeri della Dopa, Lou X, Wu Tang Clan, Nas, Gangstarr... nulla sarà più come prima.

E poi, si cresce ancora. Una notte insonne passata tra gli ipertesti di Wikipedia mi porta al nome dei 65Daysofstatic, rampa di lancio verso gli universi onirici del post rock. Intanto, i Green Heaven si sciolgono. Ricky inizia ad ascoltare e mettere dischi reggae, ma rimaniamo fratelli lo stesso. Entro in possesso del mitologico lettore MP3 Sansa Clip. Inizio a cercare solo FLAC. Conosco Beto, che ogni giorno mi tira fuori una mina nuova, assurda, a partire dai Duemanosinistra. Smetto di suonare, di fatto, ma comunque ho sempre più sete, e sono disposto a bere praticamente qualsiasi cosa.

Marnero. Radiohead. E poi chissà.
I contorni tra i generi sfumano sempre di più. 

Rimane solo il brivido nell'ascolto dei suoni perfetti, l'esaltazione nella loro ricerca, il risuonare del cuore alle parole dei testi, la curiosità nell'esplorare i sentieri della mappa.
E il volere tutto questo, sempre, ancora.
<br/>
<br/>
<br/>  

****
# **Domande infrequenti** 

### *Ma quindi da questo sito potrò scaricare musica gratuitamente?*
[No.<br/><br/>
Noi siamo per la condivisione libera dei saperi: senza questa possibilità probabilmente le nostre già ridotte conoscenze, musicali e in generale, sarebbero ancora più ridotte e neanche ci sarebbe venuto in mente di dare vita a un progetto come questo sito.<br/> Tuttavia, condividere qui la musica in modo diretto ci potrebbe potenzialmente causare una serie di problemi che non è il caso di approfondire. In ogni caso, l'internet è un posto grande e le risorse per ottenere gratuitamente ciò che si desidera sono ormai ovunque.<br/>  
Ricordiamo che per molti artisti, soprattutto quelli emergenti e indipendenti, la musica è un lavoro come un altro in cui la vendita di un disco in più o in meno potrebbe segnare in verde o in rosso il conto corrente a fine mese: vi invitiamo, quando possibile, a supportare sempre gli artisti. Per questo, in ogni articolo metteremo un link al sito ufficiale dell'artista o a Bandcamp, se disponibili, per indicarvi dove è possibile acquistare il disco.<br/> Ci limiteremo a queste due canali perché sono quelli da cui gli artisti riescono a trarre maggior guadagno, avendo ritenute nulle (nel caso di acquisto diretto dall'artista) o ridotte (nel caso di Bandcamp) da parte della piattaforma di vendita. <br/>Supportiamo gli artisti, ma facciamolo quando possibile attraverso un mercato equo della musica.](https://www.reddit.com/r/Piracy/wiki/megathread)  
<br/>
<br/>
<br/>  

****
# **Contatti** 
Sul sito non è possibile commentare: è una scelta consapevole, l'abbiamo fatta per mantenerlo più snello dal punto di vista infrastrutturale e per evitare di doverci appoggiare a piattaforme esterne.<br/><br/> Tuttavia, se voleste scriverci per consigliarci nuova musica, per salutarci, per dirci che non capiamo nulla, che il giallo vi fa schifo, che anche voi avete nostalgia dell'Unione Sovietica o per altre questioni, abbiamo creato una mail presso un provider rispettoso della privacy, indipendente e autogestito.<br/><br/> [É una figata ed è gratis, fatelo anche voi](https://disroot.org/).
<br/>
<br/>

<span class="Y">[**radio.djallo@disroot.org**](mailto:radio.djallo@disroot.org)</span>