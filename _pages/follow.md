---
layout: page
title: Sintonizzati su Radio.Djallo
---
----
<br/>
## **Ossia, come rimanere aggiornati sui nuovi articoli di Radio.Djallo senza dover spasmodicamente premere F5 sulla homepage** _(Vi abbiamo viste, non fate finta di nulla)_
<br/>
Anche nell'epoca della connessione totale e degli innumerevoli social netuors con policy di privacy discutibili tanto quanto la loro effettiva utilità, abbiamo preferito la via della semplicità e, per quanto possibile, della trasparenza.

Così come su questo sito non ci sono elementi traccianti e componenti analitici, che oltre ad aumentare il traffico di dati e quindi l'impatto energetico presentano problematiche di privacy relative alla profilazione degli utenti a fini pubblicitari [(in teoria,](https://www.ilpost.it/2013/06/07/il-caso-prism-in-10-punti/) [e poi chissà...)](https://www.ilpost.it/2018/03/19/facebook-cambridge-analytica/), anche i canali che abbiamo scelto per diffondere il sito stesso obbediscono alla stessa logica.

Attualmente, ci sono quattro modi per rimanere sintonizzati su Radio.Djallo:
<br/>
<br/>
<br/>
## La newsletter mail
I sani, vecchi, solidi metodi sono sempre i migliori.  
Grazie alla disponibilità e al lavoro che i compagnə di [Autistici/Inventati](https://www.autistici.org/) portano avanti ormai da molti anni, Radio.Djallo dispone di una newsletter sicura e privata: il vostro indirizzo mail non verrà **mai** condiviso con servizi di terze parti a fini commerciali e non vi troverete a ricevere mail improbabili su come un qualche tizio dal dubbio italiano sia riuscito a guadagnare 10.000 € al giorno stando seduto al pc, o simili.  

Riceverete una mail ogni volta che pubblicheremo un articolo e in nessun'altra occasione.  

**Come fare per iscriversi?**  
<span class="Y">**[Cliccate su questo link e seguite le istruzioni. ](https://noise.autistici.org/mailman/listinfo/radio.djallo)**</span>  

Fatto!
<br/>
<br/>
<br/>
## Il profilo Mastodon
[Mastodon è un social network di tipo federativo.](https://joinmastodon.org/)<br/><br/>
Quindi?<br/>
I social network più famosi sono ospitati su server di proprietà di un'unica entità e, di conseguenza, totalmente soggetti al solo volere di tale entità e della/delle persone che la possiedono. Un modello centralizzato: una proprietà, una volontà, e se non vi piace fatti vostri.<br/><br/>
Mastodon invece segue un modello inverso e decentralizzato: la sua rete è composta da tanti server diversi, ognuno costituente "un'istanza", una comunità, con il proprio manifesto e le proprie regole. Queste diverse comunità si possono federare tra di loro, entrando in contatto e creando una "rete di reti", molto più estesa delle singole.<br/><br/>
Non vi piacciono le regole di una determinata istanza?  
Ne cercate un'altra che rispecchi i vostri desideri.
<br/>
Noi ad esempio ci siamo iscritti all'istanza torinese [**Cisti**](https://mastodon.cisti.org/about), della quale vi invitiamo a leggere il [manifesto](https://mastodon.cisti.org/about/more).
<br/>
<br/>
<span class="Y">**Ma veniamo al pratico: come fare a seguirci su Mastodon?**</span><br/><br/>
<span class="Y">**1.**</span> Iscrivetevi a un'istanza: noi ovviamente vi consigliamo [**Cisti**](https://mastodon.cisti.org/about), ma potete iscrivervi a una qualsiasi altra istanza che sia federata a Cisti (ad esempio, una grossa istanza italiana è [Bida](https://mastodon.bida.im/about))

<span class="Y">**2.**</span> <span class="Y">[**Seguite il profilo di Radio.Djallo**](https://mastodon.cisti.org/@radiodjallo)</span>
<br/><br/>
<span class="Y">**3.**</span> **Fatto!** Benvenutə su Mastodon! Come tipologia di social network è molto simile a Twitter, ma a differenza di questo, è libero, aperto e trasparente. Per esplorare Mastodon, potete usare l'interfaccia web o <span class="Y">[una delle numerose applicazioni, disponibili per tutte le piattaforme.](https://joinmastodon.org/apps)</span>
<br/><br/><br/>
<span class="Y">**Ma non finisce qui.**</span><br/><br/>
Troppo spesso ci imbattiamo in **brani pazzeschi** che spiccano all'interno di album sui quali non scriveremmo un articolo intero. <br/>

**Non vogliamo perdere l'occasione di condividere qualcosa che ci piace, quindi abbiamo deciso di farlo con chi ci segue su Mastodon, in aggiunta alla programmazione di base.**<br/>
Dal nostro profilo, vi consiglieremo questi brani orfani, giusto per solleticare la vostra curiosità e la vostra sete di buona musica.

Un motivo in più per utilizzare Mastodon per seguirci :)
<br/>
<br/>
<br/>   
## Il feed ATOM/RSS
[Il feed ATOM](https://it.wikipedia.org/wiki/Feed) è la modalità che preferiamo perché semplice, basata su standard aperti ed estremamente efficiente: un flusso di informazioni che si aggiorna ogni volta che pubblichiamo qualcosa di nuovo, recapitandolo nell'applicazione che avete scelto per sottoscriverlo.

In pratica, è tutto molto semplice:

<span class="Y">**1.**</span> **Scegliete un'applicazione**, detta "aggregatore di feed", da utilizzare per iscrivervi al feed e **nella quale verranno recapitati i nuovi articoli.** Essendo che i feed ATOM/RSS sono basati su standard aperti, sono compatibili con **una moltitudine di applicazioni disponibili per qualsiasi piattaforma:** noi vi consigliamo **[Feeder per Android](https://play.google.com/store/apps/details?id=com.nononsenseapps.feeder.play&hl=it_IT)** e **[QuiteRSS per Windows e Mac](https://quiterss.org/en)**, ma basta **cercare "RSS" sullo store dedicato della vostra piattaforma per trovarne altre.** Scegliete quella che vi piace di più! Esistono anche degli **aggregatori online,** accessibili via browser, come [Feedly](https://feedly.com/).
<br/>
<br/>
<span class="Y">**2.**</span> Una volta installata, **aggiungete alle vostre fonti** <span class="Y">**[il link del feed ATOM di Radio.Djallo](https://radiodjallo.gitlab.io/atom.xml)**<br/>  *Se vi è più comodo, potete copiare e inserire nell'applicazione direttamente questo:<br/> **https://radiodjallo.gitlab.io/atom.xml***</span>
<br/>
<br/>
<span class="Y">**3.**</span> **Fatto!** Riceverete i nuovi articoli direttamente nell'applicazione. Notate che potete usarla anche per iscrivervi a feed di testate giornalistiche e altri siti, lo standard ATOM/RSS è molto diffuso sul web!
<br/>
<br/>
<br/>
## Il canale Telegram
[Sebbene nato dalla mente di un anarco-capitalista arricchitosi creando il social network più grande di tutta la Russia](https://it.wikipedia.org/wiki/Pavel_Durov) riteniamo [Telegram](https://telegram.org/) una piattaforma (per ora) coerente con i nostri valori relativamente all'utilizzo legato a questo sito.

Anche qui, è tutto abbastanza semplice: installate Telegram (disponibile per Android, Linux, iOS, Mac Os, Windows e da web), <span class="Y">**[andate sul canale di Radio.Djallo attraverso questo link e unitevi al canale.](https://t.me/radiodjallo)**</span>

Quando pubblicheremo un nuovo articolo, vi arriverà un messaggio.
<br/>
<br/>
<br/> 
<span class="Y">*Per ora è tutto gente :)*</span>
