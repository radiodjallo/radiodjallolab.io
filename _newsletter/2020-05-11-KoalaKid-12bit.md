---
published: true

title: ! "Kid Koala // 12 bit Blues"
description: !  "Sull'opera incompiuta"
summary: !  "Sull'opera incompiuta"
img: /img/covers/KidKoala_12bit.webp
tags:
- hip-hop  
- tourntableism  
- blues
---
<br/>
<div class="container">
<img src="/img/covers/KidKoala_12bit.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a1909814836_10.jpg">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>

<span class="didascalia">
Anno: 2012<br/>
Etichetta: Ninja Tune<br/>
[Sito Ufficiale](https://kidkoala.com/)<br/>
[Ascolta su Bandcamp](https://kidkoala.bandcamp.com/album/12-bit-blues)
</span>
<br/>
<br/>
<br/>
Alcuni dei più bei disegni di Leonardo da Vinci, incluso uno dei più famosi in assoluto, il suo autoritratto, sono dei disegni preparatori, delle bozze, in alcuni punti poco più che degli schizzi a matita.

I Prigioni di Michelangelo sono delle sculture di una potenza inaudita che il Maestro ha iniziato e mai completato, lasciando che i suoi soggetti si divincolassero disperatamente, con parti del loro corpo ancora imprigionate nella pietra grezza.

Perché queste opere d’arte incomplete, o abbozzate, ci interpellano tanto? Non dovremmo, da spettatori, pretendere delle opere finite? Che per la loro perfezione si facciano messaggere eteree del concetto che il loro autore ha voluto trasmetterci, senza che la loro incompletezza spezzi la nostra sospensione dell’incredulità? Senza essere brutalmente riportati alla nostra condizione di osservatori di macchie di colore su una tela, o di un pezzo minerale sagomato?

I passaggi intermedi di un’opera, le bozze, gli errori, sono tutti testimoni di un processo creativo, di un’abilità tecnica, che solitamente sono invisibili a chi non ne è stato partecipe.

Forse è questo che spinge alcuni architetti a non cancellare le linee costruttive dai loro progetti.

Forse è questo che spinge alcuni registi a includere, alla fine della pellicola, alcune scene imperfette girate e mai inserite nel film.

E infine, forse è questo che ha spinto Koala Kid a rilasciare 12 bit Blues.

Koala Kid è uno di quei DJ che si cimenta in una delle arti più ancestrali e virtuose dell’hip hop: il Tourntableism. Armato di due piatti e una tavola di mixaggio, con la sola destrezza dei polpastrelli, scolpisce i suoi pezzi avvolgendo e svolgendo i vinili, passando da una traccia all’altra, accarezzando con le dita sapienti i dischi neri. I brani sono dei patchwork, dei collages di suoni e samples, di crepitii e di stridenti scratch, tutti tenuti insieme da un beat.

Considero il Tourntableism, e in maniera più ampia l’hip hop strumentale, come, in qualche modo, una meta-musica, perché è creata a partire da *altra* musica, e quindi, oltre a doversi curare della *propria* estetica, del *proprio* linguaggio, dei *propri* significati, deve anche fare i conti con il materiale di base, la *sua* storia, la *sua* connotazione, la *sua* eredità. Per far bene questo genere, oltre ad essere dei maestri tecnicamente, bisogna essere degli amanti appassionati della musica. dei *sommellier* della musica.

Questa dichiarazione d’amore alla musica è un processo artistico complicato, raffinato, e il risultato finale non può essere giudicato senza aver prima intravisto l’impalcatura che regge la facciata.

Ascoltato senza essersi confrontati con il suo contesto, 12 bit Blues è poco più che una bozza, un album con un nome che pare temporaneo, con al suo interno dodici tracce che non hanno nemmeno dei nomi, che contengono pezzi di suoni, loop orfani, SCRATCHES su una base di blues sporco, abbozzato, a tratti ritmicamente imperfetto.

Ma per me 12 bit Blues è uno studio che mira alla perfezione estetica, un’opera virtuosa, in cui le parti imperfette mettono in luce la bellezza delle parti perfette. Una dichiarazione d’amore al Blues, al suo stile polveroso e grezzo, e una sua perfetta sintesi. Un disegno con le linee preparatorie ancora visibili, un blocco di marmo con solo alcune parti squisitamente scolpite.