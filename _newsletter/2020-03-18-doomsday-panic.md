---
published: true

title: ! "Chief and TheDoomsdayDevice // Panic Ruminations"
description: !  "Musiche per un cielo d'acciaio."
summary: !  "Musiche per un cielo d'acciaio."
img: /img/covers/chief-panic.webp
tags: 
- rap
- hardcore
- post-rock
- hip-hop
- noise-rap
- industrial-rap
---
<br/>
<div class="container">
<img src="/img/covers/chief-panic.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a0107696170_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2020  
Etichetta: 	Moodhouse  
[Sito ufficiale](https://chiefandthedoomsdaydevice.com/)  
[Ascolta e acquista su Bandcamp](https://moodhouse.us/album/panic-ruminationsn)
</span>
<br/>
<br/>
<br/>
Di fronte a uno schermo, una voce legge in rapida successione fotogrammi che mi mostrano un mondo decadente, malsano, tossico. Nelle orecchie quello che sembra il lamento di un leviatano elettronico in procinto di soffocare sotto la sua stessa massa in continua e tumorale espansione.

Poi Panic Ruminations termina, ma mentre riprendo a guardare alla realtà ancora risuona nelle mie orecchie come un avvertimento. O come un'ombra.

Ascolto questo album da giorni, quasi ogni ascolto accompagnato da una lettura dei testi (che sono disponibili sia su Bandcamp che sul sito ufficiale: prendete esempio, soprattutto voi rap artists) eppure, sicuramente complice anche la mia imperfetta comprensione dell'inglese quando riguarda elaborati così complessi e di natura artistica, non riesco a estrapolare interpretazioni precise da associare ai singoli pezzi. 
Ciò che mi suggeriscono le parole è invece uno scenario complessivo, claustrofobico e apocalittico: una realtà fatta di repressione, di incomprensioni, di paure sociali presenti e future e del lento degenerare della nostra mente, sempre più sola e compressa, forse anche sempre più perversa:

*dreams of turning personal trash  
into cold hard cash*

I beat mi accompagnano per mano in questa discesa: i suoni sono grezzi, elementari; *naturali*, direi. Le basi di K Death suonano infatti come la colonna sonora di questo mondo dopo la fine del mondo, agendo a un livello molto profondo: qui non ci sono melodie da apprezzare, quasi non c'è *musica*, i suoni sono suggestioni emotive e istintuali, echi che circondano la voce che declama la cortina tossica e marziale che lettera dopo lettera avvolge la nostra percezione mentre discendiamo in questo disco. 

Discesa che diventa una spirale assuefacente: questo disco non è il primo e non sarà l'ultimo disco rap distopico e alienante (mi vengono in mente Deltron 3030 e il mitico progetto italiano Artifical Kid), ma per me è sicuramente uno dei più potenti nell'essere ciò che è. 

[*Un viaggio sotto un cielo di acciaio.*](https://www.gog.com/game/beneath_a_steel_sky)