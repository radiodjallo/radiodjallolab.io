---
published: true

title: ! "Perturbator // New Model"
description: !  "“The legend says he's half human, half synthesizer...”"
summary: !  "“The legend says he's half human, half synthesizer...”"
img: /img/covers/perturbator-newm.webp
tags:
- synthwave
- metal
- retro-synth
---
<br/>
<div class="container">
<img src="/img/covers/perturbator-newm.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a3395045031_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>

<span class="didascalia">
Anno: 2017<br/>
Etichetta: Blood Music<br/> 
[Sito ufficiale](https://www.perturbator.com/)<br/>
[Ascolta, acquista o scarica gratuitamente da Bandcamp](https://perturbator.bandcamp.com/album/new-model)
</span>
<br/>
<br/>
<br/>
Uno degli aspetti interessanti della vita è che nel suo svolgersi può prendere direzioni inaspettate ma, per quanto queste possano portarci verso lidi sconosciuti, tutta l'esperienza accumulata fino a quel momento e che fa ormai parte di noi determinerà il modo in cui decideremo di condurre l'ennesima esplorazione dell'ignoto. Il nostro passato determina il nostro approccio al futuro: un approccio unico, come unica è l'esperienza di ognuna di noi.  

Così, succede che James Kent, chitarrista della band progressive metal [I The Omniscent](https://itheomniscient.bandcamp.com/) arrivato un certo momento della sua vita decide di trasformarsi in Perturbator, solista cablato ai suoi synth che diventano le corde vocali attraverso le quali inizia a narrare la storia di una società futura e distopica che assume tutti i caratteri della sua grande passione letteraria: il <span class="Y">Cyberpunk</span>.  

Ma nel suo diventare macchina, Perturbator conserva memoria del suo passato umano come James, e le dita che scorrono sui tasti e sulle manopole dei synth conservano altrettanta memoria del tempo passato a cavalcare 6 corde d'acciaio a colpi di plettro. 

E si sente.

Il sogno (o l'incubo?) di Perturbator si insinua nelle nostre orecchie con dei bassi densi, netti e striscianti, che come scosse preparatorie crepano la nostra serenità facendo filtrare urla e arpeggi sintetici. Il climax è devastante: l'apice sonoro affonda rapidamente in violenti drop che sembrano arrivare direttamente da una plettrata robotica su degli accordi densi e plumbei, valanghe di catrame e vetri dalle quali gli altri suoni cercano di fuggire disperatamente, per esplodere in trionfi melodici epici in caso di successo. Una musica cupa e violenta come la società che vuole raccontarci, che assomiglia nota dopo nota sempre più alla nostra, altrettanto cupa e violenta, società. E non è un caso che tra i diversi lavori dell'artista, uno di quelli che gli ha procurato maggiore fama sia l'aver composto parte della colonna sonora dell'ultraviolento e acclamato videogioco indie [Hotline Miami](https://hotlinemiami.com/).

La trasformazione è completa, la macchina suona e produce; ma il cuore di James batte ancora tra i suoi circuiti. 

E senza di quello, Perturbator e la sua musica non sarebbero mai esistiti in questa cruda, devastante, magnifica forma.
<br/>
<br/>
<br/>

<span class="didascalia">Una cosa che mi fa apprezzare ancora di più la buona musica? Quando questa è liberamente accessibile: il disco di Perturbator è scaricabile pagando quanto si vuole, anche nulla. Basta inserire 0 come prezzo. Così lo si può ascoltare con calma, per accorgersi che prezzo e valore  sono due cose totalmente diverse e per decidere in qualsiasi momento quale sia per noi il valore di quanto stiamo ascoltando, e a quale prezzo questo eventualmente coincida.  <br/><br/>
Ma il fatto è che, se anche volendo le nostre finanze non ci permettessero di destinare neanche un centesimo a Perturbator, potremo comunque godere della sua musica e di conseguenza arricchire noi stessi. E non è questo il verso senso dell'arte?</span>


