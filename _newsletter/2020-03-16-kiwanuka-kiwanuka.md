---
published: true

title: ! "Michael Kiwanuka // KIWANUKA"
description: !  "“Life isn’t easy. If we’re worrying about not being perfect, we’re going to really struggle.”"
summary: !  "“Life isn’t easy. If we’re worrying about not being perfect, we’re going to really struggle.”"
img: /img/covers/kiwanuka-kiwanuka.webp
tags: 
- soul
- rock
- samba
- psychedelic-rock
---
<br/>
<div class="container">
<img src="/img/covers/kiwanuka-kiwanuka.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://browse.startpage.com/do/show_picture.pl?l=english&rais=1&oiu=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F81kNoKzD-CL._SL1500_.jpg&sp=a126abcd3cbbf82c70f83e9d970a9cd3&t=blak" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2019  
Etichetta: 	Polydor - Interscope  
[Sito ufficiale](https://www.michaelkiwanuka.com/)  
[Ascolta](https://invidio.us/playlist?list=OLAK5uy_k1SFc0yntzJirDOOCQOFl2LdbE1b0bxIU)
</span>
<br/>
<br/>
<br/>
Un'esplosione di luce proietta anche ombre accecanti così come oltrepassare una montagna ci porta a salire ma anche a scendere: benvenuti nel sentiero per la redenzione dall'auto-repressione, nella strada sconnessa che conduce all'accettazione di sé; benvenuti nella chiesa di Michael Kiwanuka.

L'aspetto che amo forse di più della musica è la sua capacità di connettersi a quelle parti di noi non concesse alla parola e alla razionalità: le note ci guidano alla liberazione di frazioni del nostro essere chiuse su loro stesse, scrigni neri di emozioni rimaste sospese e irrisolte. La musica è una chiave che le libera, ce le presenta, e ci permette infine, volendo, anche di tradurle in parole. 

Questo disco mi racconta questo: il viaggio di una persona insicura (come dichiarato dallo stesso Michael) che finalmente arriva a capire di non essere il proprio problema e che il momento e il luogo migliore per essere sé stessi sono qui e adesso. E che arriva a ciò con tutti i tagli e gli inciampi dei rovi che affogano il sentiero della vita: amori complessi, schifezze e infamità come il razzismo, momenti bui come una matrioska di pozzi senza fondo.

E come, ci arriva...

La musica di Kiwanuka in dei momenti ti esplode nel cuore come un fuoco che illumina a giorno la notte, con dei passaggi da ascensione che sembrano frutto di un Jimi Hendrix sostenuto da un coro gospel di una setta brasiliana, samba e chitarre corrosive; altre volte ti trovi gettato, da solo, sul ciglio di una strada buia e sporca mentre il cielo sembra unirsi all'asfalto sotto forma di infinite gocce nere: non sai dove stai andando, ma ci stai andando da solo. 

Da un estremo all'altro su suoni di cristallo: il produttore è Danger Mouse e si sente, tutto è cesellato alla perfezione e ogni ascolto è un piacere godurioso che rivela suoni nuovi all'interno di stratificazioni lievi e preziose come foglie d'oro.  
<br/>
<br/>

Un percorso così profondo può essere descritto da una sola parola, che però è differente ogni volta; 
in questo caso, la parola è  

<span class="Y">**KIWANUKA**</span>