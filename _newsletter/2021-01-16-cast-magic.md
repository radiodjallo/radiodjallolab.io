---
published: true
title: ! "Cast // Magic Hour"
description: !  "Tuttə hanno diritto a un momento di leggerezza."
summary: ! "Tuttə hanno diritto a un momento di leggerezza."
img: /img/covers/cast-magic.webp
tags:
- rock
- indie rock
- britpop
---
<br/>
<div class="container">
<img src="/img/covers/cast-magic.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://eu-browse.startpage.com/av/anon-image?piurl=https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FI%2F61Wj7pULxpL._SL1200_.jpg&sp=1610215822T4918db877515bf32ee830f2308cfa9a2cef8f38f1ad39c53df3565a17f6b1858">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 1999  
Etichetta: Polydor  
[Sito Ufficiale](https://www.castband.co.uk/)  
[Ascolta](https://www.youtube.com/watch?v=Umgcf2iIbIQ&list=PLrKai5UlVkfZKGEMywGBnZ85FdeL9eNDv&disable_polymer=true)
</span>
<br/>
<br/>
<br/>
Da quanto abbiamo iniziato con questo sito, quella che prima era solo ricerca personale è diventata una sorta di missione, una sfida: trovare sempre suoni nuovi, band incredibili e dalle storie assurde sepolte nei più reconditi angoli dei server della rete, improbabili commistioni tra generi. La continua ricerca dell'inaspettato sonoro.<br/><br/>
Eppure, la musica non è solo questo.<br/><br/>
Così come la vita, la musica non è sempre fuochi artificiali, sorpresa e volteggi da una novità alla prossima: la musica spesso è comfort, è calore, è sicurezza. É, nel momento del bisogno, saper di poter contare proprio su quello specifico suono per raccogliere le nostre emozioni mentre sembriamo infrangerci al suolo; su quello specifico disco non eccessivamente complesso che scivola placido oliando i minuti delle nostre giornate di lavoro; su quella compilation chiassosa che in estate tira fuori il peggio di noi mentre siamo in auto con i finestrini abbassati.<br/>
Tante volte, la musica è la leggerezza che cerchiamo per far sfiatare la pressione eccessiva del mondo esterno.<br/>
E questa è una di quelle volte.<br/><br/>
I Cast non sono sconosciuti (o almeno non lo sono in patria, dove al tempo godettero di un buon successo), i loro testi non presentano particolari profondità sociali o introspettive, e la loro musica suona come una versione meno presuntuosa e più rock'n'roll di quella degli Oasis. Nessuna grande rivelazione sonora o filosofica è nascosta negli spettrogrammi di questi pezzi: solo una sensazione, quel tepore gentile tenuto per mano da una lieve brezza mentre insieme accompagnano le nostre fibre muscolari verso il rilassamento durante un assolato weekend primaverile, pronte a godersi due giorni di tregua dal lavoro salariato. Le tracce scorrono serene e luminose, donandoci quella che davvero si potrebbe dire una *magic hour* *magic 55:34 minutes*, per essere precisi).<br/><br/>
Solo una sensazione, ma che spesso vale ben più di una qualsiasi rivelazione. <br/>
Quindi,<br/><br/>
<span class="Y">godetevela.</span>