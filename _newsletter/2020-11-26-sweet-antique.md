---
published: true

title: ! "The Sweet Life Society // Antique Beats"
description: !  "CBCR, come coltivare correttamente il prato del giardino di casa"
summary: !  "CBCR, come coltivare correttamente il prato del giardino di casa"
img: /img/covers/Sweet_Antique.webp
tags:
- trip-hop
- electro-swing
- frenchtronica
- soul
---
<br/>
<div class="container">
<img src="/img/covers/Sweet_Antique.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a1850348257_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2020  
Etichetta: 	Freshly Squeezed Music  
[Ascolta e acquista su Bandcamp](https://freshlysqueezedmusic.bandcamp.com/album/antique-beats)
</span>
<br/>
<br/>
<br/>
Parlo di qualcos’altro.

I supermercati, i parchi d’attrazione a tema, i mercatini dell’usato, sono impostati in maniera molto specifica. Vengono, in alcune circostanze, definiti “non-luoghi” e sono organizzati in maniera da suggerire a chi li percorre la sensazione di non averli ancora esplorati del tutto.

Cercare musica produce lo stesso effetto in me. Posso passare ore che non ho a percorrere i corridoi virtuali infiniti, stracolmi di album, di bandcamp, spulciando, ascoltando un minuto di quello, un minuto di questo. Mi procura gran piacere l’idea di essere rimbalzato ai quattro angoli del pianeta, seguendo l’esile filo rosso delle raccomandazioni. Scoprire un dj sperimentale in qualche città russa, ed essere catapultato in Messico, o in Ontario, o Vietnam o chissà dove a scoprire un artista simile.

Le possibilità sono infinite, e sono sempre ad un passo di scoprire l’album definitivo, che placherà la mia curiosità e la mia sete di suoni.

Il mondo è vasto, le possibilità infinite, e grazie a Internet lo spazio non significa niente. Non sono più limitato da quello che posso raggiungere camminando o, al limite in bicicletta. La verità è che il mio raggio percorribile nel mondo fisico per andare ad un concerto è troppo esiguo, e le probabilità che in questo modo io scopra l’album definitivo sono infinitamente scarse.

Giusto?

Ad agosto 2017 scopro Sweet Life Society. Al Balla con i Cinghiali, su al Forte di Vinadio, al confine con la Francia. Gran festival, un po’ caro, un po’ commerciale, ma garanzia di qualità. Gli Sweet Life Society sono di Torino, la città dove sono cresciuto, amici di amici, leggermente fuori dal giro che frequentavo, e si esibivano nei locali dove sarei potuto andare, ma dove non sono mai andato. Si sono formati sull’erba del mio giardino di casa.

Agosto 2017 è stato un gran bello schiaffo in faccia, perché ho scoperto che dal giardino di casa può uscire un bell’elettro swing pulito, potente, contagioso. Un’energia e un talento live che non aveva nulla da invidiare a gruppi più famosi e più rodati. Live. Le registrazioni perdevano la spontaneità e la potenza, ma mi sono ripromesso di monitorare l’evoluzione della band. Come si usava dire quando ero giovane, con il piglio disinibito e leggermente politicamente scorretto tipico dei primi anni 2000: Cresci Bene Che Ripasso.

2020, gli Sweet Life Society rilasciano Antique Beats, concept album sperimentale che mischia con estremo buon gusto Elettro Swing, Trip Hop, Soul ed Elettronica. Un viaggio a caccia di un’estetica sonora varia, che spazia dalla Frenchtronica alle sonorità Reggae. Un viaggio che inizia fuori dalla soglia di casa mia.

Promemoria: mentre guardi quant’è verde l’erba del vicino, non dimenticare di buttare un occhio ogni tanto a quella che ricopre il tuo giardino, potrebbe valerne la pena.