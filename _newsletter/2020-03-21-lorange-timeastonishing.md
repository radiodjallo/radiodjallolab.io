---
published: true

title: ! "L'Orange // Time? Astonishing! Instrumental"
description: !  "It's a long way to the top if you wanna capire la musica"
summary: !  "It's a long way to the top if you wanna capire la musica"
img: /img/covers/Time_Astonishing.webp
tags: 
- hip-hop
- instrumental
- djing
- psychedelic
---
<br/>
<div class="container">
<img src="/img/covers/Time_Astonishing.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a0118821385_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2015  
Etichetta: Mello Music Group  
[Acquista su Bandcamp](https://lorange360.bandcamp.com/album/time-astonishing-instrumentals)  
[Ascolta su Soundcloud](https://soundcloud.com/lorangeproductions/sets/time-astonishing-instrumentals)
</span>
<br/>
<br/>
<br/>
Io credo che L’Orange sia uno di quegli artisti che per essere apprezzati richiedano un certo percorso di immersione.
Da un po’ di tempo sono all’avida ricerca di novità in quel particolare tipo di Hip Hop strumentale, fatto di collage di samples, ritmi avvolgenti, suoni profondi, atmosfere evocatrici date da crepitii di vinili rovinati e dialoghi da vecchi film. Un viaggio che raccomando a tutti quelli che riescono ad emanciparsi dai testi nella musica e, più in generale, da una struttura classica verso-ritornello.

Ho incontrato L’Orange diverse volte durante questo percorso, ma più volte non ha suscitato il mio interesse. Bisogna dire che il DJ fa cose abbastanza diverse tra loro, a seconda di chi collabora con lui o chi produce. E con gente così prolifica è facile capitare su un album in collaborazione con, per esempio, un rapper che ti distrae dalla base, o su un album così destrutturato e bizzarro da non riuscire a soffermarcisi.

Però dopo diverso tempo, durante il quale ho esplorato di più il mondo strano e vasto dell’hip hop sperimentale, sono ricapitato su L’Orange, su questo album e questa volta mi ha conquistato.

E’ una storia di viaggi nel tempo, di tecnologia e di atmosfere confuse e psichedeliche, distorte, e questa storia è raccontata da samples tronchi, deformati, distorti e glitchati, melodie quasi inesistenti, il tutto molto ricco di suoni diversi e tenuto insieme da un bel beat caloroso, lento e profondo.

Per amor di accuratezza preciso che Time? Astonishing! Instrumentals è, ovviamente, l’album strumentale di un originale Time? Astonishing! Prodotto da L’Orange per Kool Keith, ma per come la vedo io in questo caso la vera opera d’arte è la composizione del DJ del Nord Carolina, e se qualcuno ci rappa sopra mi distrae dal suo apprezzamento.

Il punto di tutto questo è che alcuni artisti non sono sempre di immediata comprensione. Anzi. Forse mi sbilancerò a dire che la musica migliore non è di immediata comprensione. Acquisisce tutto il suo senso solo quando si familiarizza con i codici del genere all’interno del quale si inserisce, con le atmosfere, con i suoni, i ritmi. Ricordate quello che avete ascoltato anni fa e non vi è piaciuto? Potrebbe essere interessante darci una nuova ascoltata, all’occasione.