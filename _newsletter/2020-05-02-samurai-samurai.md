---
published: false

title: ! "Samurai Drive // Samurai Drive"
description: !  "Blues for outer space."
summary: !  "Blues for outer space."
img: /img/covers/samurai_samurai.webp
tags:
- neo-blues
- synth-blues
- new-wave
---
<br/>
<div class="container">
<img src="/img/covers/samurai_samurai.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://e-cdns-images.dzcdn.net/images/cover/4cdf89199b990cc6d6611fc9999471ab/1200x1200-000000-80-0-0.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>

<span class="didascalia">
Anno: 2020<br/>
Etichetta: Paradise Entertainment & Distribution<br/> 
[Bandcamp](https://samuraidrive.bandcamp.com/releases)<br/>
[Ascolta](https://invidio.us/watch?v=hFRDlNWYT6Y)
</span>
<br/>
<br/>
<br/>
*Il panorama al di fuori del vetro è nero quanto infinito, innumerevoli puntini luminosi a ricordare la profondità di una visione altrimenti bidimensionale. Il ronzio dei motori e dell'elettronica accompagna l'ennesima traversata cosmica, l'ennesimo viaggio in cui l'entità più simile a una compagnia è rappresentata dal pilota automatico. Un viaggio, ma verso dove? Infinite possibilità che equivalgono a nessuna. Nell'assenza di limite ogni cosa perde senso e identità, la possibilità di comprensione di una realtà in continua espansione si allontana sempre di più, mentre l'essere umano si avvita su sé stesso in una spirale di solitario isolamento.

Tra le innumerevoli motivazioni dietro il mio amore per la musica, una è sicuramente rappresentata dalla sua capacità di diventare colonna sonora.  
Non mi riferisco solo al cinema: la musica diventa colonna sonora di piccole e grandi imprese quotidiane, di periodi positivi o negativi, di contesti e situazioni che viviamo. 
Voglio però concentrarmi sull'elemento complementare e speculare di questa sua capacità, cioè la capacità della musica di descrivere scenari immaginari, un po' come un libro sonoro. La vista è probabilmente il senso che utilizziamo con maggiore frequenza