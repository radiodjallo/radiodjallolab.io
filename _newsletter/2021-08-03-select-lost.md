---
published: true
title: ! "Select // Lost in Noise"
description: !  "Respiratore auricolare per tempi asfissianti"
summary: ! "Respiratore auricolare per tempi asfissianti"
img: /img/covers/select_lost.webp
tags:
- electro
- ambient
- elettronica
- instrumental
---
<br/>
<div class="container">
<img src="/img/covers/select_lost.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://i1.sndcdn.com/artworks-T0dffFqT4ypyvzBQ-RVlfqA-t500x500.jpg">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2021<br/>
Etichetta: egoist records<br/>
[Ascolta su SoundCloud](https://soundcloud.com/select-tunes/sets/lost-in-noise)<br/>
</span>
<br/>
<br/>
<br/>
Un virus attacca i nostri polmoni, disfacendoli come balle di fieno in un tritacarne.  
Per evitarlo, ci troviamo costretti a indossare maschere che nel salvarci, ci strozzano.  
Il caldo è opprimente, rende l'aria un liquido denso che fatichiamo ad inspirare. Ogni giorno di più.
Il mondo brucia, e i suoi fumi si insinuano nelle nostre narici.  
Ci muoviamo su mezzi che sputano catrami invisibili, e i nostri fumi si insinuano nelle nostre narici.  
Tentiamo di fuggire chiudendoci tra pareti dove scatole plastiche sputano un'aria altrettanto plastica, sebbene fresca. Sebbene il fresco di cui godiamo oggi sarà il nuovo caldo di domani.  
<br/>
<br/>
<span class="Y">Brutti tempi per chi ha l'hobby del respirare.</span>
<br/>
<br/>
<br/>
Non ci vuole una scienza per sapere che non andrà tutto bene.<br/>
Probabilmente, per usare un termine tecnico, andrà tutto in merda.<br/>
Nel frattempo però, siamo qui. Vivə.<br/><br/>
Nonostante tutto, immaginiamo.  
Progettiamo.  
Speriamo.  
Qualcunə addirittura prega.  
A qualcun altrə le mani sudavano a forza di tenerle giunte in eterno in attesa della provvidenza, quindi ha deciso di staccarle l'una dall'altra e usarle, per lottare.  
E mentre facciamo tutto ciò, mentre viviamo, cerchiamo di anche e soprattutto di sopravvivere. Di ritagliarci spazi di serenità e di semplicità, una tregua per lə dannatə in un mondo in guerra costante. Di alleggerire un carico che ci troviamo sulle spalle con il sospetto che ci abbiano messo più sassi sulla schiena di quelli che effettivamente ci spettavano.  <br/><br/><br/>
<span class="Y">Quindi,</span><br/><br/>
<br/>
mentre microscopici esseri ancora indecisi rispetto al dichiararsi parte dei viventi o no cercano di ucciderci,  
mentre le fiamme ci avvolgono,  
mentre la nostra spazzatura ci sommerge e i nostri fumi ci soffocano,
<br/>
<br/>
usiamo tutto l'arsenale che abbiamo a disposizione per ricordarci che la vita è bellissima e per questo vale la pena lottare fino all'ultima goccia di sangue per salvare la nostra, quella di chi ci è caro, quella di chi non conosciamo ma è una nostra sorella o un nostro fratello, anche se non lo sa ancora. Chi dentro al suo cuore non brama ricchezze e potere, ma felicità diffusa e universale.<br/><br/>
Ridiamo,<br/>
passeggiamo,<br/>
ci innamoriamo,<br/>
costruiamo,<br/>
<span class="Y">ascoltiamo la musica.<br/></span>
<br/>
Sono quasi sicuro che la lista possa essere decisamente più lunga, ma in fin dei conti siamo qui per l'ultimo punto.<br/>
<br/>
Ascoltiamo la musica.<br/>
Ci perdiamo nel rumore.<br/>
<br/>
In questi tempi asfissianti e opprimenti, Select ci regala una boccata di aria fresca uditiva. Si chiudono gli occhi, si preme play, e mani di velluto di accompagnano in un universo pulsante ma gentile, preciso ma delicato, sintetico ma organico. Una brezza musicale che accarezza la nostra psiche stanca, ci culla portandoci per almeno 50 minuti fuori dalla gabbia mortale che ci siamo costruiti, facendoci immaginare un mondo migliore che potrebbe esistere anche una volta tolte le cuffie.<br/><br/>
Basta rifiutare quello triste, violento e incancrenito che c'è.<br/>
Con i pensieri. Con le parole. Con i fatti.<br/><br/>
É lunga. Non sarà un pranzo di gala. Nel frattempo, godiamoci almeno questi 50 meritati minuti di leggerezza.