---
published: true

title: ! "paranoid void // Literary Math"
description: !  "Ciò che piace non si sceglie, si apprezza."
summary: !  "Ciò che piace non si sceglie, si apprezza."
img: /img/covers/paranoid-literary.webp
tags:
- math-rock
- post-rock
- jazz
- instrumental
---
<br/>
<div class="container">
<img src="/img/covers/paranoid-literary.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://cornellsun.com/wp-content/uploads/2017/11/pg-8-arts-void.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2017<br/>
Etichetta: Apple Of My Eye<br/> 
[Sito ufficiale](http://paranoidvoid.com/)<br/>
[Ascolta](https://www.youtube.com/watch?v=uLBvxQSgcAo&list=OLAK5uy_lNAaOKeREg80OLNulkK6kqUrQ8NoOdju4)
</span>
<br/>
<br/>
<br/>
La vita pone sempre molte domande.

Da dove veniamo?  
Qual'è il senso della nostra vita?  
Siamo soli nell'universo?  
<span class="Y">Perché mi piace il Math Rock?</span>  

Avendo imparato che è meglio iniziare dal semplice e andare verso il complesso, per ora mi soffermerei solamente sull'ultima domanda. Prossimamente su Radio.Djallo le risposte alle altre.

Quindi, perché mi piace il Math Rock?  
Credo sia perché riesco a rivedermici, come persona. Uno specchio.  
I ritmi spezzati e alle volte apparentemente incoerenti, l'alternarsi di passaggi rapidi a stop improvvisi che aprono a lente e riflessive passeggiate sonore su torrenti che terminano nuovamente con strapiombi di cascate di arpeggi.  
A un primo ascolto, un casino; ma ascoltando con pazienza, oltre la superficie emerge un disegno coerente, complesso, unitario che tiene insieme le pulsioni musicali anche quando queste sembrano spingere ognuna in direzione opposta alle altre. È emozionante. A tratti epico, ma anche intimo, riflessivo.  

Un equilibrio musicale fragile che richiede grande cura, attenzione e impegno per poter funzionare, per non risultare banale o inascoltabile. 

Questo mi porta a un'ulteriore domanda: perché mi piacciono le paranoid void?

Spesso mi trovo a vagare tra i dischi Math Rock di Bandcamp e grandissima parte di ciò che sento non mi comunica niente: moltissima di questa musica sembra essere "compilata" secondo gli elementi più facilmente associabili a questo genere, dando vita a un magma di dischi estremamente simili tra loro e privi di personalità.  
Math Rock (e Post Rock, aggiungerei, essendo un genere con cui condivide alcuni aspetti centrali a mio parere) mi ricordano ciò che per gli strumenti musicali è rappresentato dal basso elettrico: lo strumento più facile da suonare **male**. *Easy to learn, hard to master.*  

Ora, per descrivere le paranoid void è sufficiente prendere questo ultimo paragrafo e rigirarlo al contrario; partendo proprio dal basso elettrico.
Premettendo che tutte e tre le ragazze sono delle bombe atomiche con i rispettivi strumenti, da buon ex bassista non poteva non colpirmi il ruolo centrale che il basso ricopre dentro questo disco, in un genere in cui spesso viene sommerso da chitarre arpeggianti infuse di eco. Qui il basso si muove tra le tracce come un basilisco a caccia, pieno e consistente mentre avvolge gli altri strumenti per poi diventare squillante e letale quando parte all'attacco con degli slap e dei pop chirurgici, dando nel complesso a tutto il disco una coloritura lievemente *jazzy*. Chitarra e batteria a volte sembrano quasi aggrapparsi ad esso, delegandogli la responsabilità di scegliere dove portarle in viaggio.  
Per me, questo già basterebbe, ma c'è di più. L'intero disegno tracciato dalle nostre tre funziona, è solido ed emozionante e non nasconde mai delle sfumature j-pop che ricordano con orgoglio da dove viene questo disco e le mani di chi lo suona. La maestria con i rispettivi strumenti è sempre evidente ed è un puro piacere da ascoltare.  

Era diverso tempo che non portavo i miei timpani in queste lande dello spettro musicale. E le paranoid void mi hanno regalato un bellissimo scorcio.










