---
published: true

title: ! "Damu the Fudgemunk // Ocean Bridges"
description: !  "Jazz was revolutionary. Hip Hop is also revolutionary"
summary: !  "Jazz was revolutionary. Hip Hop is also revolutionary"
img: /img/covers/Damu_Oceans.webp
tags:
- jazz
- hip-hop
- instrumental
---
<br/>
<div class="container">
<img src="/img/covers/Damu_Oceans.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a1921292917_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2019  
Etichetta: 	Redefinition Records  
[Ascolta e acquista su Bandcamp](https://damuthefudgemunk.bandcamp.com/releases)
</span>
<br/>
<br/>
<br/>
Volevo iniziare questo articolo con qualche parola che spiegasse il rapporto tra il Jazz e l’Hip Hop, che mettesse in luce le loro similitudini, come se il secondo fosse, in un certo senso, l’erede culturale del primo. Mentre pensavo a quali parole usare, mi sono accorto che altri più legittimi e talentuosi di me lo hanno già fatto, e meglio di come lo potrei mai fare io.

>Il Jazz era rivoluzionario. E l’Hip hop anche è rivoluzionario ... Il Rap è un veicolo per fermare la violenza. Proprio come lo era il jazz ai tempi, negli anni ‘60, negli anni ‘30 ... Il Jazz è anche una conversazione segreta ... Non importa da quale cultura si provenga, sentiamo la musica e percepiamo la storia, anche non capendo le parole. Nell’Hip hop dobbiamo mettere tutto noi stessi. Così sentiamo l’energia attraversarci esattamente come un folle assolo di sassofono. E siamo qui per quello, per sentire e amare quella vibrazione.

Queste parole sono di Slimkid3, e vengono declamate, in inglese, con una sorta di slam lento con forte accento californiano alla fine del primo brano dell’album Trompe L’Oeil del trio francese Jazz Liberatorz.

Questi due generi sono imparentati, collegati tra loro da un filo doppio che li accomuna dal punto di vista culturale, politico, sociale. Questo legame non può non essere risentito nelle sonorità di entrambi. Il collegamento più naturale sta nei samples delle basi: la musica Jazz è stata depredata dei suoi samples più delicati, più avvincenti, più emotivi, e ha partecipato a plasmare l’estetica sonora se non di tutto, almeno di una parte dell’universo Hip Hop.

Ma Damu the Fudgemunk con Ocean Bridges considera un punto di vista diverso. Invece di subordinare un genere all’altro, di usare l’estetica sonora di uno per servire l’altro, riesce a portare entrambi in primo piano, perfettamente in equilibrio, dove entrambi mantengono la loro identità eppure convivono perfettamente con l’altro. In questo, il DJ si avvale della collaborazione del rapper Raw Poetic e del sassofonista Archie Shepp, entrambi presenti per assicurarsi che i generi che rappresentano non vengano corrotti o sminuiti.

Il risultato è splendido. Delicato e folle come il Jazz, ritmato e connotato come l’Hip Hop.

A Skagen, la punta più a nord della Danimarca, il mar Baltico e il mare del Nord si incontrano. A causa delle differenze di salinità, temperatura e densità dei due mari, le loro acque rimangono fieramente divise, causando forti turbolenze ma mantenendo ciascuna la propria essenza, la propria identità, separate da una lunga linea di schiuma bianca. In realtà i due mari prendono le caratteristiche l’uno dell’altro, ma in profondità, e in maniera lentissima. Questa condizione climatica eccezionale permette alle strane creature di un mare di avventurarsi e prosperare nell’altro, e viceversa.

Io credo che questo sia il senso del titolo di questo album.