---
published: true

title: ! "The Asteroid Galaxy Tour // Out of Frequency"
description: !  "Blaxploitation e Bling Bling"
summary: !  "Blaxploitation e Bling Bling"
img: /img/covers/Asteroid_Frequency.webp
tags:
- pop
- alternative-rock
- psychedelic
---
<br/>
<div class="container">
<img src="/img/covers/Asteroid_Frequency.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="http://m.blog.hu/wo/wobe/image/a4850d429fb89465ce37f901d49a1ef6.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2012  
Etichetta: 	BMG Rights  
[Sito Ufficiale](https://theasteroidsgalaxytour.com/)  
[Ascolta su YouTube](https://www.youtube.com/watch?v=QIJYRvguofY)
</span>
<br/>
<br/>
<br/>
Uno degli aspetti più affascinanti e misteriosi della musica è il suo potere evocativo. Inteso in tutti i suoi sensi.

In primo luogo, quando questo si lega alla nostra personale esperienza. La musica ha il potere di portarci indietro nel tempo. A me basta, per esempio, sentire soltanto qualche nota di qualche brano dei Simple Plan per essere trascinato a forza indietro nel tempo, verso quegli oscuri anni della mia adolescenza, fatti di primi amori non corrisposti, peluria facciale indesiderata, paura dell’isolamento sociale e una generale e indefinibile ansia per il futuro che mi ostino a ricordare come un periodo idilliaco della mia giovinezza.

La musica ha anche il potere di farci viaggiare nello spazio, ed esplorare e conoscere luoghi che non abbiamo mai realmente visto e che forse non vedremo mai. Il beat crudo e la voce acida di Williamson degli Sleaford Mods, che ci evoca le immagini brutali dei bassifondi proletari londinesi; così come il magico flauto di Ian Anderson dei Jethro Tull, che ci porta alle praterie fatate dell’irlanda occidentale.

In ultimo, la musica ha il potere più ampio di creare atmosfere, stati d’animo, che in vita nostra magari non avremo mai l’occasione di sperimentare altrimenti. Questi stati d’animo sono, io credo, ancora una volta molto soggettivi, ma spesso certi suoni, certi strumenti o certi ritmi hanno la capacità di generare immagini collettivamente accettate e condivise. Penso per esempio a un soft jazz, che ha il potere di evocare immediatamente un raffinato salottino anni quaranta, con tutto il corredo di piume di pavone, guanti di velluto nero e bocchini da sigaretta che ne consegue. Oppure alla sguaiata elettronica dei primi album dei Prodigy, che se ne viene con i suoi grandi capannoni industriali abbandonati pieni di fumo, luci stroboscopiche verdi e danze indiavolate di corpi sudati.

Questo potere straordinario di portarci in luoghi dove non siamo mai stati è intensamente sfruttata dall’industria cinematografica. Non potremmo immaginare un film senza una corretta e potente colonna sonora, ad enfatizzare e farci provare quello che sarebbe altrimenti meramente mostrato sullo schermo, ad enorme distanza da noi. La fantastica relazione tra la musica e il cinema è così ricca e sfaccettata che, con il tempo, ha permesso la nascita di generi musicali che si *ispirano* alle colonne sonore, ma che di fatto sono emancipate dalla loro controparte visiva. Penso per esempio ai Calibro 35 che, se ascoltati con gli occhi chiusi, ci buttano dentro alla trama incalzante di un vecchio giallo italiano degli anni cinquanta.

Ma qui voglio parlare di qualcosa di un po’ diverso.

Gli Asteroid Galaxy Tour, tanto per cambiare, fanno un genere che, sulla carta, non mi attira particolarmente. Certo, il Funk, il Soul. Ma quel genere di Funk Soul talmente imbevuto in una salsa pop, leccata e coperta di lustrini che di solito mi smorza l’entusiasmo. Eppure fin dal primo ascolto di “The Golden Age”, che non a caso è poi il singolo che ha sfondato internazionalmente, sono rimasto affascinato dal suo potere di trasportarmi in un luogo dove non avrei mai pensato di trovarmi. Un misto tra un Red Carpet da serate degli Oscar e lussuosi lounge alla Coco Bongo in The Mask, per intenderci (e se non ci intendiamo penso che sia arrivata l’ora per voi di andare a riguardare The Mask). Nessuno dei due luoghi mi si addice particolarmente, ma sono rimasto affascinato dalla *precisione* con cui mi venivano trasmesse quelle atmosfere così poco familiari a me.

Poi, facendo qualche ricerca, ho scoperto che questi richiami culturali così poco familiari sono legati alla Blaxploitation, uno stile cinematografico a basso budget che portava sullo schermo tematiche legate alla comunità afroamericana agli inizi degli anni settanta, e che si attorcigliavano a trame da pulp polizieschi. Tutto questo, prima di leggerlo e capirlo, in qualche modo lo avevo *sentito* ascoltando Out of Frequency di The Asteroid Galaxy Tour.

Una band Danese peraltro.