---
published: true

title: ! "Kassa Overall // I THINK I'M GOOD"
description: !  "Il peso della malattia mentale"
summary: !  "Il peso della malattia mentale"
img: /img/covers/Ithinkimgood.webp
tags:
- jazz
- hip-hop
- future-fusion
---
<br/>
<div class="container">
<img src="/img/covers/Ithinkimgood.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a2262581880_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>

<span class="didascalia">
Anno: 2020  
Etichetta: 	Brownswood Recordings  
[Sito Ufficiale](https://www.kassaoverall.com/)  
[Ascolta e acquista su Bandcamp](https://kassaoverall.bandcamp.com/album/i-think-im-good)
</span>
<br/>
<br/>
<br/>
A volte la chiave per capire un album è capire il musicista e l’uomo che lo ha composto. Anche in questo senso il contesto conferisce significato e valore all’opera, la arricchisce.

Diverse cose saltano all’orecchio subito, appena si comincia ad ascoltare i primi brani di I THINK I’M GOOD. Sicuramente la prima è il suo *peso*. Ogni brano è *pesante*. Difficile spiegare in che senso. In ogni brano questo peso si manifesta in maniera leggermente diversa, e per me questo è una dimostrazione della complessità dell’album e del virtuosismo di Kassa. Il *peso* è espresso da rumori di fondo che riempiono il silenzio tra le note, dalla voce saturata di chi canta troppo vicino al microfono, dal virtuoso tappeto di batteria jazz, disorganizzata e presa dal panico solo in apparenza, dai suoni leggermente disagevoli di un correttore vocale troppo marcato.

Questo acquisisce tutto il suo senso quando si scopre che Kassa Overall con questo album parla di un passato violento, di una vita in prigione e del taboo culturale di parlare apertamente della propria iperemotività e della propria instabilità mentale.

Esteticamente, I THINK I’M GOOD sembra un tentativo di approcciarsi a tanti generi diversi senza sentirsi troppo a proprio agio con nessuno di essi. Kassa è un virtuoso jazzman classico alle prese con generi contemporanei. E a questi egli sembra approcciarsi in maniera puramente accademica. Come se ne avesse studiata approfonditamente la teoria senza averne mai sentito una nota, e ci si cimentasse con uno stile personale. Questo, così scritto, può lasciar pensare che il risultato sia grottesco e ridicolo, invece io credo che questa sia una magnifica opportunità per guardare la musica che conosciamo e che amiamo o detestiamo sotto una luce diversa, con occhi nuovi, contemplando cosa sarebbe potuto essere.