---
published: true

title: ! "Moses Sumney // Aromanticism"
description: !  "“If you can’t question the inherent oppressive nature of society, I don’t want you in my life.”"
summary: !  "“If you can’t question the inherent oppressive nature of society, I don’t want you in my life.”"
img: /img/covers/moses_aro.webp
tags:
- folk
- experimental
- indie
- pop
- soul
- elettronica
---
<br/>
<div class="container">
<img src="/img/covers/moses_aro.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a0763917463_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2017<br/>
Etichetta: Jagjaguwar<br/> 
[Sito ufficiale](https://www.mosessumney.com/)<br/>
[Ascolta e acquista su Bandcamp](https://mosessumney.bandcamp.com/album/aromanticism)
</span>
<br/>
<br/>
<br/>
Il livello di sensibilità di alcune persone alle volte mi spaventa.   
Non in assoluto, ma in relazione alla violenza del nostro mondo: empatizzo, e mi chiedo come sia possibile, quando si è dotate di una sensibilità spiccata, riuscire a reggere il carico di violenza oppressiva intrinseco a questa forma di società.

Infatti, alcuni non ci riescono. Molte, troppi, troppe anche si stesse parlando di una sola persona. Altri sì. *I sommersi e i salvati*, diceva qualcuno. 

Come si diventa *salvate*? Come si rimane a galla? Bella domanda. Complessa. Forse, si tratta per ognuno di trovare la propria quadra. Un compromesso, un equilibrio tra comprensione, accettazione, opposizione, azione. Azione, espressione: sicuramente, un modo per non *affondare* è gettare zavorra, trovare dei modi di esternare la pesantezza per ridurre il carico.

La musica.  

Questo disco è una farfalla pesante quanto un buco nero. Un macigno, ma delicato.  
Un falsetto dolce, chitarre e bassi che paiono carezze sulla pelle e un contorno di suoni altri ad avvolgerli come se il suono viaggiasse assopito su una nuvola.
Ma la nuvola è nera, pesante, colma di un temporale: Moses Sumney ci racconta il suo viaggio personale verso l'accettazione dell'essere *aromantico*, del faticare a ritrovarsi nelle forme e nei ruoli propri delle relazioni così come definite da norme sociali spesso non esplicitate ma non per questo meno rigide. 

>***Aromantic***
>1. Commonly describes someone who experiences little to no romantic attraction, abbreviated to aro.<br/>
It also describes someone ﻿whose experience of romance is disconnected from normative societal expectations, due to feeling repulsed by romance, or being uninterested in romantic relationships.<br/><br/>
>2. Commonly used as a specific identity term by people who experience no romantic attraction.
><footer>Definizione di "Aromantico" dell'AUREA - Aromantic-spectrum Union for Recognition, Education, and Advocacy.</footer>

Una solitudine intrinseca, interiore, avvolta dalla solitudine sociale di chi rifiuta di recitare una parte che non sente sua e lo fa senza senso di colpa, anzi con fierezza: questo non è un disco che si limita alla dimensione dell'espressione e della liberazione personale. 
Questo è un disco politico, un disco che rivendica la possibilità di essere fragili, di poter scrivere le **proprie regole** per godersi la **propria vita**, un disco di denuncia delle relazioni asimmetriche fondamentali nella riproduzione della società capitalista patriarcale nella quale viviamo.

[*Da sommersi, o da salvati?*](https://www.thefader.com/2017/09/04/moses-sumney-aromanticism-interview)


