---
published: true

title: ! "Dominique Fils-Aimé // Nameless"
description: !  "Il silenzio è sempre lo strumento più difficile da suonare."
summary: !  "Il silenzio è sempre lo strumento più difficile da suonare."
img: /img/covers/dominique-nameless.webp
tags:
- neo-soul
- gospel
- r&b
---
<br/>
<div class="container">
<img src="/img/covers/dominique-nameless.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a2522694736_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>

<span class="didascalia">
Anno: 2018<br/>
Etichetta: Ensoul Records<br/> 
[Sito ufficiale](https://domiofficial.com/en/)<br/>
[Ascolta e acquista su Bandcamp](https://singwithmi.bandcamp.com/album/nameless)
</span>
<br/>
<br/>
<br/>
Credo di dover ammettere a me stesso che la ricerca dell'essenzialità sia un qualcosa che sta guadagnando di importanza nella mia vita, in ciò che faccio, in ciò che ascolto.

È qualcosa di forte, che muove dal profondo. L'alleggerimento da tutti gli orpelli, dal superfluo, la bellezza nella sua forma più sintetica e concentrata che si trova in quel momento in cui nulla più si può togliere, pena il perdere l'equilibrio che rende vivo ciò che si è appena creato. Meraviglioso.

Attenzione: essenziale non significa scarno, o povero, o obbligatoriamente minimale. Semplicemente, essenziale è ciò che utilizza al meglio la minor quantità di risorse necessaria per raggiungere un obiettivo. Nessuno spreco, nessun fronzolo.  

<span class="Y">*Nameless.*</span>  

Dominique Fils-Aimé ha un piano, un piano decisamente ambizioso. Vuole ripercorrere la storia della musica black scrivendo una trilogia di dischi. Questo è il primo, e ovviamente parte dalle origini: il blues, le *work songs* cantate dagli schiavi.  

<span class="Y">*Nameless.*</span>  

La voce di Dominique accende un sole rovente, condensa un'aria soffocante inumidita soltanto da un sudore carico di rabbia e disperazione, ordina dei fili di paglia in un giaciglio su cui coricarsi mentre la luce tramonta. Contrabbasso, batteria, archi  intervengono delicati nella realtà costruita dalla voce come un soffio di vento o il frinire di un grillo, dettagli spesso minuti e ignorati ma fondamentali nel rendere *vero* lo scenario che appare di fronte alle nostre orecchie.  

Un equilibrio delicatissimo, un rischio altissimo: un colpo di tamburo di troppo, una nota in meno dal contrabasso, e tutto collasserebbe. Troppo pieno, o troppo scarno; troppo banale, o troppo sperimentale. E invece no. Non succede mai. 8 tracce, ognuna fondamentale, nessuna ridondante.

Ogni cosa è al suo posto, diretta da un uso magistrale di uno degli strumenti musicali più importanti in assoluto: **il silenzio.**  

<span class="Y">*Occhi chiusi, cuffie in testa, luce spenta, tasto play. Grazie Dominique.*</span>