---
published: true
title: ! "Svalbard // It's Hard To Have Hope"
description: !  "Melodia o barbarie."
summary: ! "Melodia o barbarie."
img: /img/covers/svalbard_hard.webp
tags:
- post punk
- post hardcore
- post rock
- hardcore
---
<br/>
<div class="container">
<img src="/img/covers/svalbard_hard.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a0639100130_10.jpg">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2018  
[Ascolta e acquista su Bandcamp](https://svalbard.bandcamp.com/album/its-hard-to-have-hope)
</span>
<br/>
<br/>
<br/>
Tra le varie, amo la musica pesante. Veloce. Violenta. É liberatoria. 
Ci sono momenti in cui si è prossimi a un collasso emotivo e psicologico, un'implosione imminente derivante da un sovraccarico di energia che non abbiamo possibilità di trasformare immediatamente e che si prepara ad avere la meglio su di noi. In quei momenti, quella musica così poderosa e incontrollabile fluisce nel mio corpo come un balsamo salvifico attraverso le cuffiette, due valvole che liberano quella massa di materia oscura intrappolata e prossima all'esplosione e ridanno alle cellule la possibilità di respirare.

Ciò che invece non amo è l'assenza di armonia, di melodia, il rumore scoordinato. Il problema non è nel peso e nella violenza sonora delle singole parti, ma nella profondità e nell'attenzione data al processo di metterle insieme. In quel cosmo di mezzo, la differenza tra il rumore gratuito e l'espressione infuocata delle proprie emozioni. Il problema non è la violenza in sè: il problema èe la violenza senza progetto, che diventa soltanto gratuita dissipazione di energia.

*It's Hard To Have Hope* è pesante, in tutto. A partire dal titolo. É veloce. É, legittimamente e genuinamente, violento, nella musica e nei testi. Le chitarre scorrono rapidissime come fulmini che tagliano il cielo, mentre affianco a loro cavalca instancabile la batteria con il suo "tupatupatupa" hardcore arrestato solo dalle briglie di bridge e ritornelli improvvisi. In tutto ciò, le voci urlano, più forte possibile, contro alcune delle più immonde schifezze che ci troviamo a vivere in questo *migliore dei mondi possibili*. Un disco di rabbia pura, assolutamente legittima, che non perde però mai di lucidità. Gli Svalbard sbandano a destra e a sinistra durante la loro cavalcata sonora quasi accecati dal furore che li guida, ma non escono mai dalla carreggiata, non finiscono mai nel burrone della cacofonia: c'è sempre una coerenza armonica, una melodicità che tiene i timpani incollati alle cuffie, i battiti del cuore sincronizzati alla grancassa. E l'attenzione su quello che sta succedendo. Non solo nelle nostre orecchie.

Perché questo è un altro elemento fondamentale di *It's Hard To Have Hope*: i testi. Nessuno spazio per la poesia, per le metafore, per le figure retoriche: le parole degli Svalbard sono esplicite, crude e violente tanto quanto il mondo che descrivono. Questo è un aspetto che raramente riesco ad apprezzare in quei dischi dal contenuto politico i cui testi, appunto, paiono comunicati di partito, senza alcun elemento di astrazione artistica. Ma qui abbiamo un'eccezione: un disco che non vuole fare compromessi, e dalla musica alle parole decide di essere un lucido urlo di odio e rabbia chirurgicamente indirizzati. Niente generalismi antisistemici posati in nome di una radicalità di facciata: le tracce di questo disco si dedicano tutte a problemi specifici e assolutamente reali, che la musica che accompagna i testi ci trasmette con la rabbia che tuttə sentiamo dentro.

*It's Hard To Have Hope* non è un disco di favole o di slogan: è un disco dell'oggi.
E oggi, *It's Hard To Have Hope*.
Ma *It's Hard To Have Hope* è certamente un disco di rabbia.
La rabbia può evolvere in rassegnazione, o in progetto.
Il progetto è speranza.

A noi la scelta.

<span class="Y">*When are we going to save what we’ve got?*</span>