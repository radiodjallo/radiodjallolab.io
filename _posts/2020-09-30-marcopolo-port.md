---
published: true

title: ! "Marco Polo // Port Authority"
description: !  "This is feel-good music."
summary: !  "This is feel-good music."
img: /img/covers/marcop_port.webp
tags:
- hip-hop
- rap
---
<br/>
<div class="container">
<img src="/img/covers/marcop_port.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://eu-browse.startpage.com/av/anon-image?piurl=https%3A%2F%2Fimg.discogs.com%2FzI5NJTuMYXdgp3nI1-Lgy11i5tI%3D%2Ffit-in%2F600x600%2Ffilters%3Astrip_icc%28%29%3Aformat%28jpeg%29%3Amode_rgb%28%29%3Aquality%2890%29%2Fdiscogs-images%2FR-980474-1180310631.jpeg.jpg&sp=1601491602T4cc2fb3f47bc99ad43ad33da0e01c781addc505121b8ce56428515f92ad7496b" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2007  
Etichetta: Soulspazm - Rawkus Records  
[Ascolta e acquista su Bandcamp](https://marcopolobeats.bandcamp.com/album/port-authority)
</span>
<br/>
<br/>
<br/>
Per la serie "articoli che iniziano descrivendo un aspetto che amo della musica", un aspetto che amo della musica è la sua capacità di superare le barriere razionali che più o meno consapevolmente ci imponiamo, sorprendendo e anticipando il pensiero nel soddisfare quello che è il nostro reale desiderio di ascolto.  
<br/>
Stare dietro come si deve a questo sito non è facile come poteva sembrare all'inizio, come il declino nella continuità delle pubblicazioni vi avrà fatto notare. Nonostante le premesse date a noi stessi, di farlo per passione e divertimento e di non trasformarlo in una gabbia d'obbligo, era inevitabile che un castello di aspettative e di ansia da prestazione andasse a posarsi mattone dopo mattone sulle spalle di chi per quanto sia cazzone cerca comunque di mettere sempre il meglio e il cuore in ciò che fa.  
Così si entra nel cunicolo buio dell'inadeguatezza perenne, che blocca l'azione e logora il pensiero: la ricerca musicale inizia a scollarsi dal quel semplice, puro e salutare desiderio di godere della bellezza e benessere sonori, andando a incagliarsi nella ricerca dell'album più particolare, meno conosciuto, più sperimentale. Il *digging* digitale, da processo sincero ed equilibrato diventa l'equivalente di un controllo qualità a livello industriale, che vede passare tra le orecchie migliaia di suoni cercandone uno che risponda a quei parametri prestazionali e iper-razionali che ci siamo posti all'avvio del macchinario ansiogeno ma ai quali la musica, per la sua natura irrazionale, emotiva, non risponderà mai in modo soddisfacente. L'ascolto diventa un processo meccanico al quale sfugge il piacere dell'ascolto stesso, e di conseguenza la bellezza di quanto passa stiamo *sentendo*. Così, la vite si avvita senza fine in un vortice di insoddisfazione che in modo paradossale e doloroso nel cercare musica allontana dal piacere di ascoltarla.
<br/>
<br/>
<br/>
Poi, un giorno ti capita di dover chiedere in prestito l'auto a tua madre dopo un lungo stop dalla guida; nel cassetto del cruscotto, ancora al suo posto, il porta cd con mix più o meno improbabili in formato audio puro, perché l'autoradio della Clio terza versione si sognava MP3, Bluetooth e altri metodi e formati di fruizione dell'audio. Quei cd, fossili offline di un'era passata, sono immuni al sovraccarico di informazioni a cui ci espone il collegamento 24/7 alla rete. Così, i 30 secondi di attesa per l'apertura del cancello del garage diventano occasione sfogliare il raccoglitore, e dopo 10 minuti di ricerca davanti al cancello ormai aperto da tempo, la scelta è fatta: il sottile oggetto di plastica con su scritto male a mano "Marco Polo" scivola placido nel cruscotto dell'auto e "Track 01" appare sul display.  
Ed è in quel momento che la musica ti guida per mano, ti porta fuori dall'oblio ricordandoti la bellezza dell'ascoltarla.
In una selva intricata fatta di rigidi parametri autoimposti, freddi, razionali e paralizzanti, i beat di Marco Polo irrompono come una bomba nucleare che in un istante ha fatto tabula rasa riportandomi davanti all'amore per la musica nella sua forma pura e primordiale. Il produttore canadese intesse trame di suoni che ti entrano nel cuore puntuali e profondi come un defibrillatore attaccato alla testa di un treno in corsa verso il tuo torace. Il sarto sonoro è bravo, e infatti ogni base è un vestito perfettamente a misura di ognuno degli artisti che compongono il roster top di gamma dei forgiatori di barre presenti su questo disco e che leggiadri o pesanti, fini o grezzi, ballerini o schiacciasassi vanno a chiudere in cerchi perfetti le fondamenta poste dal buon Polo, Marco Polo. Marco Polo è l'Action Man che arriva con il machete a salvarmi dalla giungla di suoni sperimentali ma per me inascoltabili nella quale mi ero perso e dalla quale non pensavo di voler più uscire. Attenzione però: non c'è nulla di banale o di semplice nella sua opera. Come nei migliori pezzi d'arte, la tradizione del passato, la sensibilità del presente e l'orecchio puntato al futuro trovano una sintesi equilibrata e perfetta: la riscossa del buono, mai vecchio, ma sempre sano, rap fatto come si deve, con quella capacità personale che non tutti hanno, in qualsiasi tipo di arte, di far elevare i loro prodotti dalla massa dandogli un nome e un cognome precisi: Port Authority.
