---
published: true

title: ! "Phoniks // Down to Earth"
description: !  "Il genere meno impegnativo"
summary: !  "Il genere meno impegnativo"
img: /img/covers/68_viper.webp
tags:
- lo-fi
- downtempo
- chillout
- trip-hop
---
<br/>
<div class="container">
<img src="/img/covers/Phoniks_Down.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a3457864381_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2018  
Etichetta: Don't Sleep Records  
[Ascolta e scarica gratuitamente su Bandcamp](https://phoniksbeats.com/album/down-to-earth)
</span>
<br/>
<br/>
<br/>
Suoni rovinati, liquidi quasi. Sample di batteria funk, ma lentissimi. Crepitii. Voci soavi, che parlano come attraverso una vecchia radio. Scratch e glich con parsimonia, loop infiniti. E quegli stramaledetti campanellini, che spargono ovunque le loro gocce sonore.

Questo è il lessico sonoro, questa è la grammatica alla base di Down to Earth dei Phoniks. Ma in realtà questa è la grammatica di uno dei generi più codificati. Un genere che polarizza all’ascolto, e che divide tra chi lo trova insignificante e scialbo, senza personalità e troppo semplice da realizzare perché possa avere una qualche dignità artistica; e chi rimane rapito dai suoi codici, imprigionato nella sua ripetitività infinita, come in uno stato di trance, di sospensione della realtà, in una bolla fragile fatta di suoni del passato, eterna eppure sempre sul punto di scoppiare.

Gli ultimi tempi sono stati difficili per tutti, ce ne sono tra di noi che hanno dovuto, qualcuno per la prima volta, imparare a star fermi invece di muoversi, ad ascoltare invece di parlare, ad interrogarsi invece di capire. E allora perché no, per qualcuno potrebbe essere arrivato il momento anche di cambiare idea su qualche genere musicale, come è successo a me. Ebbene sì, oggi parliamo di Downtempo.

Come quasi tutti, ho scoperto il Downtempo, con le sue atmosfere chillout e i suoi suoni lo-fi, a metà strada tra il beat acid jazz e l’ambient, scrivendo una qualunque combinazione di questi termini su youtube e cliccando su una delle compilation da un’ora o più. Come promesso dai titoli, erano perfette per studiare, perché l’impegno richiesto per ascoltarle è minimo. Ma il mio interesse per questo genere si è fermato qui per molto tempo. Nessuna originalità, nessuna differenza tra un brano e l’altro, e comunque il fatto di trovare così tante raccolte che non hanno nemmeno la dignità di portare un nome solitamente non è garanzia di qualità.

Ultimamente però, mi sono trovato a dover far fronte a un insolito quantitativo di ansia e stress, e mi sono trovato in una nuova predisposizione psicologica. 

E ho capito qualcosa di nuovo. La musica è importante, è il piacere che solletica il secondo più importante senso che abbiamo, in grado di stimolare sia la nostra mente che il nostro corpo; evoca enigmi da decifrare, ricordi dimenticati. È garante di infiniti piaceri, infinite interpretazioni, infinite emozioni.

Ma al confine di questa tempesta di stimoli, di questo mondo denso ed intricato da esplorare, si trova un oceano placido, dove i significati sfumano dolcemente a favore di una forma unicamente estetica. E per una volta non c’è niente da *capire*, niente da *interpretare*, niente di niente, solo lasciarsi cullare alla deriva, all'infinito. E per una volta, una volta sola, liberati dal fardello d’ogni responsabilità nei confronti della Musica, dell’Arte, possiamo solo lasciare che la mente sia libera di riposarsi, e il corpo con essa.
