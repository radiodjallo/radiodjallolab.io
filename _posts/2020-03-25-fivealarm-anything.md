---
published: true

title: ! "Five Alarm Funk // Anything is Possible"
description: !  "“Some bands want to change the world. Five Alarm Funk’s goal is much simpler. They want you to sweat.”"
summary: !  "“Some bands want to change the world. Five Alarm Funk’s goal is much simpler. They want you to sweat.”"
img: /img/covers/fivealarm_anything.webp
tags: 
- funk
- gipsy
- afro funk
- latin
---
<br/>
<div class="container">
<img src="/img/covers/fivealarm_anything.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a1867010470_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2010  
Etichetta: 	fivealarmfunk  
[Sito Ufficiale](http://www.fivealarmfunk.com/)  
[Ascolta su Bandcamp](https://fivealarmfunk.bandcamp.com/album/anything-is-possible)
</span>
<br/>
<br/>
<br/>
Inizio questo articolo con una piccola premessa: quando ho deciso di parlare dei Five Alarm Funk su questo sito mi sono trovato un po’ in difficoltà. Ho ascoltato molto due loro album in questo ultimo periodo, questo di cui parlo ora e Sweat. Ora, li trovo entrambi molto buoni, e li ho ascoltati diverse volte per cercare di capire quale dei due meritasse più dell’altro una recensione, senza riuscire a decidermi. Certo, Anything is Possible ha un arrangiamento funk della Bourrée, pezzaccio di Bach al quale sono molto affezionato per colpa del flauto di Ian Anderson (dei Jethro Tull). Però, ecco... Sweat ha una cover così epicamente migliore di quella di Anything is Possible. Avrei potuto scrivere un articolo intero sulla copertina di Sweat, della sua gloriosa inappropriatezza come cover di un album funk, dei meccanismi che ci portano ad associare certe estetiche a certi generi musicali, dell’immaginario squisitamente Lovecraftiano che a me evoca curiosamente il film John Carter di Andrew Stanton, e di quanto quel film sia ingiustamente disprezzato. Poi, mentre l’articolo si stava pian piano prendendo forma nella mia testa mi sono ricordato che questo sito tratta la musica, e in maniera solo marginale della grafica delle cover. Quindi pazienza, vada per Anything is Possible e per la Bourrée. Prenderete un momento più tardi per andare ad apprezzare quel capolavoro insensato di cover, e a concordare con me che sarebbe più appropriata per un album di Death Metal (en passant raccomando ovviamente anche l’ascolto di Sweat).

Anything is Possible, dunque, come ci annuncia lo strillo all’inizio del primo brano, subito dopo un coro bianco che, anche qui, confonde le acque non preannunciando minimamente il tono del resto dell’album. Ho scoperto i Five Alarm Funk nel modo in cui tutti sognamo di scoprire la musica: vagando a caso sul web, senza cercare nulla di particolare. Prima incuriosito dalla cover (di Sweat), poi intrigato dall’inizio, e poi, a mano a mano che i brani si susseguivano, sempre più affascinato.

Penso che ciascuno di noi abbia delle preferenze estetiche trasversali a tutti i generi che apprezza. Per qualcuno, che ascolta esclusivamente un certo tipo di Hard Rock, o di Elettronica, è magari più facile: la velocità, uno strumento, un suono caratteristico... Per altre persone con un ascolto più ampio e vario può essere più complicato trovare degli elementi comuni tra i diversi generi. Però spesso ce ne sono, e penso sia importante per ciascuno individuarli, perché permettono di capire cosa cerchiamo, in senso meno superficiale, e a prescindere dai generi e dagli stili, nella musica. E questo può aiutare in primo luogo a esplorare più facilmente la musica al di fuori della nostra “zona di comfort” senza essere troppo spaesati, ma soprattutto, in secondo luogo, ci dà indicazioni sul nostro carattere e delle chiavi di lettura per capire noi stessi un po’ meglio.

Per me, uno di questi elementi trasversali che, a prescindere dallo stile, mi porterà sempre ad apprezzare un brano, è la Patchanka.

Il termine, coniato da Manu Chao, indica un genere “patchwork” composto da elementi presi in prestito dal Rock, dal Reggae e da musiche tradizionali di diverse culture, oltre che dei testi in lingue diverse anche all’interno dello stesso brano. In senso più largo, considero che il termine Patchanka possa servire ad indicare un modo di fare musica molto dinamico, che cambia continuamente a livello di ritmo, di melodia e di atmosfera. Ecco cosa accomuna Mano Negra, Chlorine Free, Chinese Man, Rage Against the Machine, Synapson e Five Alarm Funk, bands che fanno generi totalmente diversi fra loro, ma che hanno un modo di mantenere viva l’attenzione tramite virtuosismi costanti, bruschi cambiamenti e infinite influenze da altri generi.

Anything is Possible si costruisce seguendo una specie di crescendo scostante, che volendo semplificare si potrebbe riassumere così: Coro da chiesa, voci bianche, poi rock, blues, funk, jazz, poi giusto un tocco di musica balcanica, e poi metal e congas fino al brano Brodway.

Mi soffermo un minuto su questo brano in particolare, innanzitutto perché è il brano che preferisco, e in secondo luogo perché è abbastanza emblematico dell’atmosfera frenetica e virtuosa dell’intero album. Inizia sottotono con un funk più pulito, poi intervengono le percussioni che trasformano il brano in qualcosa di quasi latino. Ancheggiando arrivano gli ottoni, teatrali come al solito, che ci evocano un universo di big band, solo alcuni svolazzi di chitarra elettrica preannunciano quello che verrà più tardi. Assolo di percussioni. Poi ripartono gli ottoni, e giusto mentre il tuo cervello comincia a rilassarsi perché finalmente ha capito la struttura del brano, cambio radicale, diventa tutto più scuro e metallico, più pesante. E giusto mentre ti chiedi che diavolo c’entra questo con il pezzo che c’era prima arriva lui, Bach, assolutamente fuori luogo e al tempo stesso così pertinente.

E niente, io mi emoziono quando un brano mi sorprende così tante volte.