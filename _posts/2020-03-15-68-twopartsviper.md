---
published: true

title: ! "'68 // Two Parts Viper"
description: !  "“My struggles are long so it's a good thing my attention span is so short”"
summary: !  "“My struggles are long so it's a good thing my attention span is so short”"
img: /img/covers/68_viper.webp
tags:
- grunge
- post-hardcore
- rock
---
<br/>
<div class="container">
<img src="/img/covers/68_viper.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a1974044924_10.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2017  
Etichetta: 	Good Fight, Cooking Vinyl  
[Ascolta e acquista su Bandcamp](https://68music20.bandcamp.com/album/two-parts-viper-deluxe-edition)
</span>
<br/>
<br/>
<br/>
C'è un momento in cui i glitter spariscono, i filtri di Instagram e gli adesivi di Facebook vanno in crash, l'idea che ogni cosa andrà da sola per il verso giusto si dissolve e tutto ciò che rimane è uno specchio sincero e tagliente che riflette la nostra vita e funziona all'inverso di un contemporaneo ritratto di Dorian Gray: ignorarlo porta a dissolverci in un magma indefinito privo di dolore, ma anche di identità; affrontarlo brucia ma alla fine dei conti ci fa guadagnare il nostro nome e la nostra persona. 
E capisci che quello specchio che riflette la tua personale vita rappresentà di per sé la vita in generale: non tutto andrà bene, non tutto andrà male, ma ci sarà da sudare.

Vivo così questo album dei '68, a torto o ragione, sebbene pare che al cantante Josh Scogin non importi molto di essere frainteso.

> "At the end of the day, if people don’t understand where I am coming from I think that would be okay."
><footer>Josh Scogin riguardo la possibilità che i suoi testi vengano interpretati diversamente dalla sua visione</footer>

Ascoltando questo disco, mi viene da immaginare una realtà parallela in cui i Nirvana sono riusciti a viaggiare nel tempo intatti fino ai giorni nostri e arrivando, dopo aver dato un'occhiata all'intorno, avessero detto *"Bella merda. Vabbè, diamoci da fare."*. E, capite due cose del mondo avessero fatto un nuovo disco. Un grunge dei giorni nostri che ha recepito dei suoni e delle finezze forse ancora poco diffuse negli anni '90. E che, pur avendo questa ricordo delle sue origini, ha un'identità propria nelle parole e nella musica.

I suoni e i testi di questo album sono a volte molto duri, ruvidi, urlati al mondo o forse a una persona; altre volte invece è come ci fossero dei momenti di raccoglimento, più intimi e riflessivi, dove la ruvidezza rimane ma è parte di una lama latente che lentamente ci attraversa da parte a parte; altri pezzi sembrano avere il tono consapevole ma non rassegnato dei compromessi che ogni giorno facciamo per cercare di difendere il nostro grammo di felicità. 

Consapevole ma non rassegnato: Scogin è cristiano, e si vede, soprattutto nell'ultima traccia. Non so con quale intensità lo sia, a malapena so che faccia abbia, ma la sua musica a me racconta di una religione che parla di spirito e non di dogmi, e in questo mi ritrovo pur non essendo credente (sperando di non scoprire poi che è in realtà un estremista antiabortista et similia).

*We can wage war against the war of hate   
At least in my dreams, I still believe we can*

Anche io credo ancora sia possibile.  
Non tutto andrà bene, non tutto andrà male, ma ci sarà da sudare.