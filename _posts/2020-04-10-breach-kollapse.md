---
published: true

title: ! "Breach // Kollapse"
description: !  "Se noi cambiamo, perché le cose dovrebbero rimanere sempre le stesse?"
summary: !  "Se noi cambiamo, perché le cose dovrebbero rimanere sempre le stesse?"
img: /img/covers/breach-kollapse.webp
tags:
- hardcore
- post-hardcore
- metalcore
- metal
- post-rock
---
<br/>
<div class="container">
<img src="/img/covers/breach-kollapse.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://eu-browse.startpage.com/do/show_picture.pl?l=english&rais=1&oiu=https%3A%2F%2Fmetal.academy%2Fuploads%2Freleases%2Fb2b869a9433cca001445182d78cb44cf.jpg&sp=54da29dfb1ff6aef628293a882452fd4&t=default" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2001  
Etichetta: Burning Heart Records  
[Ascolta](https://www.albumsdepot.com/albums/breach-kollapse/)
</span>
<br/>
<br/>
<br/>
Uno dei tratti che ricerco con più assiduità nella musica che ascolto è la capacità di sorprendermi.  

A livello microscopico, amo quei passaggi sonori agili, scaltri, *inaspettati* che riescono a tirarmi fuori dei sorrisi assolutamente spontanei mentre penso alla genialità della combinazione musicale che mi è appena passata per le orecchie;

a livello macroscopico, amo le band e i musicisti che riescono a sorprendermi nel loro percorso, che riescono a uscire dalla loro stessa ombra e fare un disco che non ti saresti aspettato da loro ma nel quale comunque riesci a sentire la loro storia, il loro percorso.

C'è da dire, su questo piano le cose non vanno sempre bene: forse alle volte cerchiamo nel nuovo disco di un'artista semplicemente una nuova dose di quanto già conoscevamo e apprezzavamo di lei; cerchiamo un ascolto *rassicurante*, confortevole, una conferma.  
Eppure delle volte penso a quanto tutto ciò possa rappresentare qualcosa di innaturale. Addirittura preoccupante, forse.  
Noi cambiamo sempre, cambiamo in ogni momento, cresciamo e maturiamo: come potremmo produrre sempre le stesse cose? Se la musica che un artista produce è specchio di sé, produrre musica sempre uguale vuol forse dire che quella persona ha smesso di crescere, di cambiare? Di farsi domande su se stessa? Come fanno gli AC/DC a dormire sereni consapevoli di aver prodotto 16 album uguali l'uno all'altro? Ma lo sono, consapevoli? 

Non so.   
Ma in questo specifico caso, poco importa perché questi non sono a parer mio problemi dei Breach.

Prima di scrivere questo articolo, ho fatto un viaggio a ritroso nella loro discografia. Sono partito proprio da questo disco, il primo che ho conosciuto grazie a un fortuito Shazam aspettando il concerto di una delle band più emozionanti che abbia mai sentito e della quale parlerò sicuramente in futuro... ma tornando a noi, sono partito dall'ultimo disco. Dal 2001 sono andato indietro fino al 1994, perché volevo sentire, volevo capire chi fossero questi Breach, da dove venissero i suoni di Kollapse, che mi stavano catturando così tanto.

Così, nel mio viaggio ho conosciuto una band metalcore con dei caratteri e delle intuizioni interessanti e apprezzabili anche dai non fanatici del genere (come me), una band che circa ogni due anni sfornava un disco nuovo aggiungendo sempre una spezia in più, una nuova finezza che ne definiva sempre meglio l'identità distinguendola dagli oceani di band tanto veloci, brutali e violente tanto quanto anonime. Qualche disco, due EP, ed è il 2001.

Kollapse.
*L'inaspettato.*

Gli spettrogrammi a blocco solido tipici di quei pezzi saturi, monolitici sia a livello ritmico che sonoro lasciano spazio a... allo spazio stesso, al vuoto, ai silenzi: alle sane, violente e "classiche" scariche più hardcore si alternano e mescolano ritmiche quasi tribali, arpeggi tesi e note allungate come lamenti di leviatani feriti; ascoltare Kollapse è come danzare leggeri su un prato di rasoi affilati... cadute incluse.


I Breach non hanno più prodotto dischi, dopo questo.  
Nel 2007 hanno fatto un concerto reunion, che li ha visti distruggere i loro strumenti a fine show. Forse Kollapse sarà il loro ultimo disco in assoluto. Si nasce, si cresce, si muore. 

E da lì, qualcosa rinasce.