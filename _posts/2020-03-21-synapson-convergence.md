---
published: true

title: ! "Synapson // Convergence"
description: !  "Per non giudicare gli album dalla copertina."
summary: !  "Per non giudicare gli album dalla copertina."
img: /img/covers/synapson_convergence.webp
tags: 
- electro
- nu-disco
- djing
---
<br/>
<div class="container">
<img src="/img/covers/synapson_convergence.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://i1.sndcdn.com/artworks-000176225386-4qzehs-t500x500.jpg" class="linkbox">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2015  
Etichetta: Warner Music  
[Ascolta su Soundcloud](https://soundcloud.com/synapson/sets/convergence-album)
</span>
<br/>
<br/>
<br/>
Per la mia esperienza è difficile giudicare e valutare un certo brano senza ricorrere alle euristiche preparate dal nostro cervello.

Detto male, le euristiche sono scorciatoie, percorsi predefiniti con i cui il nostro cervello associa automaticamente concetti per risparmiare energia e accorciare il tempo di elaborazione.

Questa lunga e fosca introduzione per dire, certo in maniera artificialmente complessa, che se tutta la vita ho odiato il reggaeton, e tutti i miei amici di facebook hanno sempre scritto male del reggaeton, è difficile che non mi si arriccino le dita dei piedi solo a sentire le prime parole in portoghese sul ritmo cadenzato tipico del genere. 

Le euristiche, come quasi tutto quello che ci ha regalato l’evoluzione nel corso dei millenni, non è una cosa cattiva. Per esempio fa sì che io non debba ascoltare ogni singola canzone di Jul per sapere che, con ragionevoli probabilità calcolate dal mio cervello, non me ne piacerà nessuna e passerò tutto il tempo a rivalutare il concetto di estinzione programmata della razza umana. In questo modo mi risparmierò il supplizio e potrò dedicarmi ad attività più costruttive.

Al tempo stesso però credo sia sbagliato abusare di questi processi mentali. Per citare mia madre quando ho scoperto gli MMORPG (se non sai cos’è chiedi al tuo amico nerd), che mi diceva “un pochino va bene ma non esagerare”. E questo consiglio vale per un sacco di cose, dal BDSM, alcune droghe, le battute offensive. Il punto è che se esageriamo a classificare la musica in generi “che ci piacciono” e “che non ci piacciono” finisce che perdiamo il senso delle sfumature, delle zone grigie di transito tra un genere e l’altro, l’incredibile varietà di stili, i modi di suonare, le voci, gli ibridi, gli esperimenti, lo ying nello yang, le metafore scontate e tutte quelle cose che ci fanno dire che, alla fine, è stupido e un po’ ridicolo voler davvero suddividere la musica in generi.

Quindi qualche giorno fa stavo passeggiando su alcuni siti dove scopro musica nuova, e sono capitato su questo album. Prima ancora di ascoltare qualunque cosa la copertina non mi ha fatto una grande impressione. Però sempre mia mamma dice di non giudicare gli album dalla copertina, quindi ho iniziato ad ascoltare lo stesso. I Synapson mi hanno subito aggredito le orecchie con un beat secco, pulsante e sgraziato, sovradimensionato rispetto al resto del contenuto sonoro.

Ora, voglio essere molto chiaro con voi: io sono cresciuto tra i metallari, dove l’avversione per la musica elettronica, la Tecno, la House e il Lento Violento è stata incisa al laser nel mio codice genetico. Avoglia a liberarti da certi preconcetti. Tutta quella fetta di produzione musicale non mi ha mai interessato, l’ho ascoltata poco e non ho gli strumenti per parlarne, ma ho sempre avuto la tendenza a pensare che un beat troppo forte, ignorante, e delle basi melodiche ultra semplicistiche e stereotipate fossero un grave sintomo di pigrizia compositiva. Il ponto è che Convergence ha tutto per essere disprezzato da uno come me.

Eppure l’ho ascoltato, tutto, tante volte, e c’è qualcosa di genuino al suo interno. In qualche modo il beat troppo marcato, le melodie troppo semplici, i suoni troppo artificiali, sono al servizio della musica. Sono una precisa scelta artistica, non scivolano mai nella facilità di composizione, e sono equilibrati da elementi puntuali prestati da altri generi che restituiscono la profondità del lavoro che sta dietro. E da un punto di vista estetico funzionano.

E insomma, questo album è bastato a farmi rimettere in discussione una parte delle mie certezze e forse ha aperto uno spiraglio su una stanza che non avrei pensato di visitare mai. Uno passa una vita a costruirsi un gusto, a capire cosa vole ascoltare e cosa no. E’ un lavoraccio, ma bisogna anche essere pronti a buttare tutto giù e ricominciare da capo.