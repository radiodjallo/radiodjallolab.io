---
published: true
title: ! "The Crooked Fiddle Band // Another Subtle Atom Bomb"
description: !  "Artista è chi con uno strumento ordinario è in grado di creare qualcosa di straordinario."
summary: ! "Artista è chi con uno strumento ordinario è in grado di creare qualcosa di straordinario."
img: /img/covers/Crooked_Another.webp
tags:
- prog-rock
- post-rock
- chainsaw-folk
- nordic-folk
---
<br/>
<div class="container">
<img src="/img/covers/Crooked_Another.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://f4.bcbits.com/img/a0318054254_10.jpg">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2019  
Etichetta: Bird's Robe Records  
[Sito Ufficiale](http://www.crookedfiddleband.com/)  
[Ascolta e acquista su Bandcamp](https://crookedfiddleband.bandcamp.com/album/another-subtle-atom-bomb)
</span>
<br/>
<br/>
<br/>
La musica è un solido complesso che può essere guardato sotto numerose luci, ed ogni volta proietta un’ombra radicalmente diversa. 

Possiamo pensare al musicista come ad un progettista, che compone un brano organizzando suoni e silenzi su una linea temporale.

Possiamo pensare al musicista come ad un visionario, in grado di percepire accordi e melodie che toccano il cuore e l’anima.

Di rado però pensiamo al musicista come ad un artigiano.

Ho scoperto The Crooked Fiddle Band per caso, come quasi sempre, forse ispirato dalla sua copertina, e mi sono tuffato in un album ben più materico di quello a cui potevo essere preparato.

Another Subtle Atom Bomb è un album artigianale, materico, fatto di legno e ottone, di corde battute, sfregate, pelli tese e vibrazioni.

Gli strumenti di Crooked Fiddle Band sono degli oggetti fisici, con un peso, un volume, e vengono toccati dai loro musicisti… ooh se vengono toccati! Il violino di Jess Randall viene mulinato in tutte le direzioni in un complesso ed acrobatico gioco erotico, ed il risultato è una gamma di suoni che va ben al di là di quello che ci si potrebbe aspettare da un pezzo di legno, quattro corde ed un archetto. Lo stesso vale per il contrabbasso, le percussioni e la chitarra, e per la selezione di strumenti della tradizione folk nordica: la nykelharpa, la cetra ed il bouzouki.

The Crooked Fiddle Band sono a tutti gli effetti degli artigiani, che spendono anni ad esplorare i loro strumenti in ogni anfratto, e restituiscono la loro esperienza sotto l’incredibile forma di un folk nordico potente e ricco, mischiato con atmosfere dense post e prog-rock; un equilibrio unico che appartiene loro e che hanno battezzato Chainsaw Folk.

È facile rinchiudersi in una bolla, nella comfort zone del proprio genere preferito, ed è anche giusto così: bisogna esplorare ciò che si ama, conoscerlo a fondo. Non bisogna però perdere di vista il confine della bolla, la sottile mebrana che separa quello che sai da quello che non sai. Il rischio non è di perdersi solamente un artista o un genere, ma di non avere l’occasione di vedere l’altra ombra proiettata; una natura della musica nuova, che insieme alle altre ne crea tutte le sfaccettature.

L’istinto, la spontaneità, l’agilità manuale e la chimica del rapporto con lo strumento. La musica è anche parti che si toccano, sfregano, vibrano e rimbombano.