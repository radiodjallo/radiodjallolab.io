---
published: true
title: ! "Michael McCann // Deus Ex: Human Revolution OST"
description: !  "Stimolanti sonori per un futuro presente."
summary: ! "Stimolanti sonori per un futuro presente."
img: /img/covers/mccann_deusex.webp
tags:
- OST
- videogiochi
- elettronica
- orchestrale
- drum'n'bass
- trip hop
- instrumental
---
<br/>
<div class="container">
<img src="/img/covers/mccann_deusex.webp" alt="Cover del disco" class="cover" style="width:100%">
<div class="textbox">
<div class="text">
<a href="https://is4-ssl.mzstatic.com/image/thumb/Music123/v4/bd/31/ba/bd31ba7b-23e5-514e-5686-ba28f9675d8c/669311301727_cover.jpg/1000x1000bb.webp">Vai all'immagine originale</a><br/><br/><span class="smalltext">Comporta un maggiore<br/>trasferimento di dati!<br/><br/>Vedi la pagina LOW.TECH.WEB<br/>per ulteriori informazioni.</span>
</div>
</div>
</div>
<br/>


<span class="didascalia">
Anno: 2011  
Etichetta: Skill Tree Records  
[Sito Ufficiale](https://michaelmccann.io/)  
[Ascolta e acquista](https://michaelmccann.io/projects/deus-ex-human-revolution-soundtrack/)
</span>
<br/>
<br/>
<br/>
E finalmente, eccoci qua.  
Se parliamo di musica, non possiamo guardare solo alla punta dell'iceberg, la musica per l'ascolto "fine a se stesso", ignorando l'enorme massa di tesori che rimane molto spesso in profondità, sotto il pelo dell'acqua: le colonne sonore.  
E se, al sentire questo termine,  la testa di moltə potrebbe già essere in viaggio verso scenari composti di pellicole, set e liste di film da vedere prossimamente in streaming, mi permetto di interrompere subito il processo.  
Via i popcorn unti, attaccate tastiera e mouse (o il gamepad, a seconda delle vostre preferenze): i suoni di oggi non accompagnano le gesta compiute da strapagati umani ripresi di fronte a sottili dischi di vetro; bensì, le storie di sculture di poligoni che vivono in mondi di pixel e texture, mondi enormi e vibranti, eppure sottili quanto i segnali elettrici di cui sono composti.  
Colonne sonore per un'arte ancora troppo scarsamente considerata, in questo paese più che in altri: i videogiochi. Ebbene, ancora molte, sicuramente troppe persone forse non sanno che i videogiochi non sono solo FIFA o il puzzle gratuito scaricato dallo store del vostro smartphone e pieno di pubblicità insistenti che stanno uccidendo la batteria del vostro telefono tanto quanto la vostra privacy digitale: sono artefatti che tanto quanto i libri, i film (o gli album, ovviamente) sono in grado di racchiudere mondi complessi e meravigliosi, spunti riflessivi semplici e profondi quanto lame nella nostra coscienza, narrazioni articolate e dilemmi etici e morali.   

Proprio come in questo caso.  
Human Revolution è il terzo capitolo nell'universo di Deus Ex, una saga ambientata in un futuro (o meglio, in una versione alternativa del presente, dato che si parla dell'anno 2027 e entro quell'anno, a parte il deterioramento dell'ecosistema, non riesco a vedere analogie tra il nostro livello di sviluppo e quello del gioco) in cui lo sviluppo della tecnologia biomedica è arrivato a livelli tali per cui è diventato possibile potenziare il corpo umano tramite "innesti" artificiali elettromeccanici. Gambe, occhi, braccia, mani... Per chi può permetterselo, il mercato (governato da poche mega-corporazioni, in piena aderenza con gli stilemi cyberpunk) offre le più svariate possibilità di potenziamento umano... se di umani si può ancora parlare. Chi lo decide? Chi lo controlla? Chi stabilisce le regole, e perché? Queste le domande con le quali ci confrontiamo durante l'avventura, domande che anche nella realtà sembrano essere giorno dopo giorno sempre più attuali.  
Mentre proseguiamo nella storia, interrogandoci sul profondo significato di umanità e sui suoi confini, Michael McCann ci accompagna con dei suoni ibridi, proprio come i personaggi che incontriamo. Su corpi di simil-archi e vocalizzi e di batterie a volte incessanti, altre di un minimalismo orientale e quasi tribale, si innestano sonorità digitali e fraseggi elettronici, in un'integrazione che è perfettamente funzionante ma non è completa fusione, proprio come gli innesti elettromeccanici del mondo di Deus Ex rimangono visibili sui corpi dei *potenziati*. Questo il tratto distintivo che permane ma che, trattandosi di una colonna sonora, si declina traccia per traccia obbedendo alle necessità del contesto: per le strade della *mega-city* cinese di Hengsha le tracce acquistano un tipico sapore orientale, mentre Detroit porta nelle nostre orecchie i suoni ansiogeni di una metropoli meccanizzata e opprimente.  
Qualunque sia il luogo o la situazione in cui o per cui suona, McCann mantiene un profilo mutevole che si adatta alle richieste dei nostri timpani.Trovo molte colonne sonore difficili da ascoltare in modo "assoluto", fuori dal prodotto di cui sono, appunto, colonna sonora: soprattutto per quanto riguarda le colonne sonore dei videogiochi, spesso ci si trova dentro loop brevi che annoiano rapidamente se ascoltate con attenzione maggiore a quella datagli giocando. La soundtrack di Deus Ex: Human Revolution invece si adatta alle nostre esigenze di ascolto del momento: se gli si presta attenzione, è sempre in grado di regalare un dettaglio sonoro in più; se invece la si mette come sottofondo, stimola piacevolmente il nostro cervello senza mettersi di traverso.  

Insomma: che dobbiate salvare il mondo o scrivere un documento, schiacciate Play e godetevi la spinta.