## Radio.Djallo // La D è muta, parla la musica.

La musica è forse l’unica forma d’arte dalla quale è impossibile sfuggire.  

A differenza delle arti visive, della musica fruiamo anche nostro malgrado, appena entriamo in un negozio, quando la sera andiamo in un pub o accendiamo la radio. In mezzo a questa moltitudine di stimoli è difficile identificare e coltivare un gusto proprio, che ci appartenga. Paradossalmente, in un mondo dove la produzione e la condivisione musicale è alla portata di tutti, è difficile trovare il tempo e la voglia di affrontare le ore di ricerca e di ascolto necessarie per scovare un brano, un album, un artista o un genere che davvero ci tocca e ci trasmette qualcosa di profondo, qualcosa che davvero apprezziamo. 

L’apprezzamento musicale è un meccanismo davvero complesso, influenzato da molti fattori: dov’eri quando hai sentito per la prima volta *quel* pezzo? Cosa facevi? Lo associ a qualcosa, o a qualcuno? Lo hai amato subito o hai imparato ad apprezzarlo con il tempo? Noi pensiamo che l’apprezzamento musicale sia imprescindibile dal contesto, sia quello che ci ha avvicinati a una musica, sia quello che ha permesso a quella musica di esistere. Come quando un amico ti parla con entusiasmo di un film, sarai più suscettibile, se non di amarlo, almeno di guardarlo con più attenzione e trovarlo interessante. Questo è l’obiettivo di questo sito. 

Parleremo di musica.

Della musica che ci piace, di quella che non ci piace, di quella che consigliamo invitando tutti ad ascoltare con maggiore attenzione e coinvolgimento, nella speranza che forse, grazie a noi, qualcuno possa scoprire qualcosa che gli corrisponda, scoprendo qualcosa in più di sè stesso.

[**radio.djallo@tuta.io**](mailto:radio.djallo@tuta.io)

----

[**Radio.Djallo è basato sul tema Colorie di Ronalds Vilcins**](https://github.com/ronv/colorie)
